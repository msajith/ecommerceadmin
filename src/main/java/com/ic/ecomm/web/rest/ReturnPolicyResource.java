package com.ic.ecomm.web.rest;

import com.ic.ecomm.domain.ReturnPolicy;
import com.ic.ecomm.repository.ReturnPolicyRepository;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link com.ic.ecomm.domain.ReturnPolicy}.
 */
@RestController
@RequestMapping("/api")
public class ReturnPolicyResource {

    private final Logger log = LoggerFactory.getLogger(ReturnPolicyResource.class);

    private static final String ENTITY_NAME = "returnPolicy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReturnPolicyRepository returnPolicyRepository;

    public ReturnPolicyResource(ReturnPolicyRepository returnPolicyRepository) {
        this.returnPolicyRepository = returnPolicyRepository;
    }

    /**
     * {@code POST  /return-policies} : Create a new returnPolicy.
     *
     * @param returnPolicy the returnPolicy to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new returnPolicy, or with status {@code 400 (Bad Request)} if the returnPolicy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/return-policies")
    public ResponseEntity<ReturnPolicy> createReturnPolicy(@RequestBody ReturnPolicy returnPolicy) throws URISyntaxException {
        log.debug("REST request to save ReturnPolicy : {}", returnPolicy);
        if (returnPolicy.getId() != null) {
            throw new BadRequestAlertException("A new returnPolicy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReturnPolicy result = returnPolicyRepository.save(returnPolicy);
        return ResponseEntity.created(new URI("/api/return-policies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /return-policies} : Updates an existing returnPolicy.
     *
     * @param returnPolicy the returnPolicy to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated returnPolicy,
     * or with status {@code 400 (Bad Request)} if the returnPolicy is not valid,
     * or with status {@code 500 (Internal Server Error)} if the returnPolicy couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/return-policies")
    public ResponseEntity<ReturnPolicy> updateReturnPolicy(@RequestBody ReturnPolicy returnPolicy) throws URISyntaxException {
        log.debug("REST request to update ReturnPolicy : {}", returnPolicy);
        if (returnPolicy.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ReturnPolicy result = returnPolicyRepository.save(returnPolicy);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, returnPolicy.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /return-policies} : get all the returnPolicies.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of returnPolicies in body.
     */
    @GetMapping("/return-policies")
    public List<ReturnPolicy> getAllReturnPolicies() {
        log.debug("REST request to get all ReturnPolicies");
        return returnPolicyRepository.findAll();
    }

    /**
     * {@code GET  /return-policies/:id} : get the "id" returnPolicy.
     *
     * @param id the id of the returnPolicy to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the returnPolicy, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/return-policies/{id}")
    public ResponseEntity<ReturnPolicy> getReturnPolicy(@PathVariable String id) {
        log.debug("REST request to get ReturnPolicy : {}", id);
        Optional<ReturnPolicy> returnPolicy = returnPolicyRepository.findById(UUID.fromString(id));
        return ResponseUtil.wrapOrNotFound(returnPolicy);
    }

    /**
     * {@code DELETE  /return-policies/:id} : delete the "id" returnPolicy.
     *
     * @param id the id of the returnPolicy to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/return-policies/{id}")
    public ResponseEntity<Void> deleteReturnPolicy(@PathVariable String id) {
        log.debug("REST request to delete ReturnPolicy : {}", id);
        returnPolicyRepository.deleteById(UUID.fromString(id));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
