package com.ic.ecomm.web.rest;

import com.ic.ecomm.config.Constants;
import com.ic.ecomm.domain.OrderLineItem;
import com.ic.ecomm.repository.OrderLineItemRepository;
import com.ic.ecomm.security.jwt.TokenProvider;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.jsonwebtoken.Claims;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link com.ic.ecomm.domain.OrderLineItem}.
 */
@RestController
@RequestMapping("/api")
public class OrderLineItemResource {

    private final Logger log = LoggerFactory.getLogger(OrderLineItemResource.class);

    private static final String ENTITY_NAME = "orderLineItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderLineItemRepository orderLineItemRepository;
    
    private final TokenProvider tokenProvider;

    public OrderLineItemResource(TokenProvider tokenProvider,OrderLineItemRepository orderLineItemRepository) {
        this.orderLineItemRepository = orderLineItemRepository;
        this.tokenProvider = tokenProvider;
    }

    /**
     * {@code POST  /order-line-items} : Create a new orderLineItem.
     *
     * @param orderLineItem the orderLineItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderLineItem, or with status {@code 400 (Bad Request)} if the orderLineItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-line-items")
    public ResponseEntity<OrderLineItem> createOrderLineItem(@RequestBody OrderLineItem orderLineItem) throws URISyntaxException {
        log.debug("REST request to save OrderLineItem : {}", orderLineItem);
        if (orderLineItem.getId() != null) {
            throw new BadRequestAlertException("A new orderLineItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderLineItem result = orderLineItemRepository.save(orderLineItem);
        return ResponseEntity.created(new URI("/api/order-line-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-line-items} : Updates an existing orderLineItem.
     *
     * @param orderLineItem the orderLineItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderLineItem,
     * or with status {@code 400 (Bad Request)} if the orderLineItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderLineItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-line-items")
    public ResponseEntity<OrderLineItem> updateOrderLineItem(@RequestBody OrderLineItem orderLineItem) throws URISyntaxException {
        log.debug("REST request to update OrderLineItem : {}", orderLineItem);
        if (orderLineItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderLineItem result = orderLineItemRepository.save(orderLineItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderLineItem.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /order-line-items} : get all the orderLineItems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderLineItems in body.
     */
    @GetMapping("/order-line-items")
    public List<OrderLineItem> getAllOrderLineItems(@RequestHeader (name="Authorization")String token) {
        log.debug("REST request to get all OrderLineItems");
        
    
        
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
        	token =  token.substring(7, token.length());
        }
        Claims claims = tokenProvider.getClaims(token);
        
        String tenantId = claims.get(Constants.TENANT_ID).toString();
        String merchantId = null;
        String storeId = null;
        if (claims.containsKey(Constants.MERCHANT_ID)){
        	merchantId = claims.get(Constants.MERCHANT_ID).toString();
        }
        if (claims.containsKey(Constants.MERCHANT_ID)){
        	storeId = claims.get(Constants.STORE_ID).toString();
        }
        
        if(merchantId != null && storeId!= null ) {
        	 return orderLineItemRepository.findByMerchantAndStore(tenantId, merchantId, storeId);
        }else if (merchantId != null) {
        	return orderLineItemRepository.findByMerchant(tenantId, merchantId);
        }else if(storeId != null) {
        	return orderLineItemRepository.findByStore(tenantId, storeId);
        }
        return null;//orderLineItemRepository.findAll();
    }

    /**
     * {@code GET  /order-line-items/:id} : get the "id" orderLineItem.
     *
     * @param id the id of the orderLineItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderLineItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-line-items/{id}")
    public ResponseEntity<OrderLineItem> getOrderLineItem(@PathVariable String id) {
        log.debug("REST request to get OrderLineItem : {}", id);
        Optional<OrderLineItem> orderLineItem = orderLineItemRepository.findById(UUID.fromString(id));
        return ResponseUtil.wrapOrNotFound(orderLineItem);
    }

    /**
     * {@code DELETE  /order-line-items/:id} : delete the "id" orderLineItem.
     *
     * @param id the id of the orderLineItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-line-items/{id}")
    public ResponseEntity<Void> deleteOrderLineItem(@PathVariable String id) {
        log.debug("REST request to delete OrderLineItem : {}", id);
        orderLineItemRepository.deleteById(UUID.fromString(id));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
