package com.ic.ecomm.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ic.ecomm.domain.ProductVendorClient;
import com.ic.ecomm.repository.ProductVendorClientRepository;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.ic.ecomm.domain.ProductVendorClient}.
 */
@RestController
@RequestMapping("/api")
public class ProductVendorClientResource {

    private final Logger log = LoggerFactory.getLogger(ProductVendorClientResource.class);

    private static final String ENTITY_NAME = "productVendorClient";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductVendorClientRepository productVendorClientRepository;

    public ProductVendorClientResource(ProductVendorClientRepository productVendorClientRepository) {
        this.productVendorClientRepository = productVendorClientRepository;
    }

    /**
     * {@code POST  /product-vendor-clients} : Create a new productVendorClient.
     *
     * @param productVendorClient the productVendorClient to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productVendorClient, or with status {@code 400 (Bad Request)} if the productVendorClient has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-vendor-clients")
    public ResponseEntity<ProductVendorClient> createProductVendorClient(@RequestBody ProductVendorClient productVendorClient) throws URISyntaxException {
        log.debug("REST request to save ProductVendorClient : {}", productVendorClient);
        if (productVendorClient.getId() != null) {
            throw new BadRequestAlertException("A new productVendorClient cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductVendorClient result = productVendorClientRepository.save(productVendorClient);
        return ResponseEntity.created(new URI("/api/product-vendor-clients/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-vendor-clients} : Updates an existing productVendorClient.
     *
     * @param productVendorClient the productVendorClient to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productVendorClient,
     * or with status {@code 400 (Bad Request)} if the productVendorClient is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productVendorClient couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-vendor-clients")
    public ResponseEntity<ProductVendorClient> updateProductVendorClient(@RequestBody ProductVendorClient productVendorClient) throws URISyntaxException {
        log.debug("REST request to update ProductVendorClient : {}", productVendorClient);
        if (productVendorClient.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductVendorClient result = productVendorClientRepository.save(productVendorClient);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productVendorClient.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-vendor-clients} : get all the productVendorClients.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productVendorClients in body.
     */
    @GetMapping("/product-vendor-clients")
    public List<ProductVendorClient> getAllProductVendorClients() {
        log.debug("REST request to get all ProductVendorClients");
        return productVendorClientRepository.findAll();
    }

    /**
     * {@code GET  /product-vendor-clients/:id} : get the "id" productVendorClient.
     *
     * @param id the id of the productVendorClient to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productVendorClient, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-vendor-clients/{id}")
    public ResponseEntity<ProductVendorClient> getProductVendorClient(@PathVariable String id) {
        log.debug("REST request to get ProductVendorClient : {}", id);
        Optional<ProductVendorClient> productVendorClient = productVendorClientRepository.findById(UUID.fromString(id));
        return ResponseUtil.wrapOrNotFound(productVendorClient);
    }

    /**
     * {@code DELETE  /product-vendor-clients/:id} : delete the "id" productVendorClient.
     *
     * @param id the id of the productVendorClient to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-vendor-clients/{id}")
    public ResponseEntity<Void> deleteProductVendorClient(@PathVariable String id) {
        log.debug("REST request to delete ProductVendorClient : {}", id);
        productVendorClientRepository.deleteById(UUID.fromString(id));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
