package com.ic.ecomm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.ic.ecomm.domain.enumeration.OrderStatus;

/**
 * A OrderLineItem.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_ORDER_DETAILS")
public class OrderLineItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "ORDER_NUMBER")
    protected String orderNumber;

    @Column(name = "VENDOR_COMPANY_ID")
    protected String vendorCompanyId;


    @Column(name = "PAYMENT_DATE")
    protected LocalDate paymentDate;

    @Column(name = "TAX")
    protected BigDecimal tax;

    @Column(name = "SHIPMENT_FEE")
    protected BigDecimal shipmentFee;

    @Column(name = "TOTAL_AMOUNT")
    protected BigDecimal totalAmount;

   
    @Column(name = "DELIVERED_DATE")
    protected LocalDate deliveredDate;

    @Column(name = "PRODUCT_NAME")
    protected String productName;

    @Column(name = "TRACKING_ID")
    protected String trackingId;

    @Column(name = "TRACKING_URL")
    protected String trackingUrl;

    @Column(name = "CLIENT_ID")
    protected String clientId;

    @Column(name = "EMAIL")
    protected String email;

    @Column(name = "RESET_TOKEN")
    protected String resetToken;

  
    @Column(name = "TIME_STAMP")
    protected Instant timeStamp;

    @Column(name = "ORDER_DETAIL_ID")
    protected Integer orderDetailId;

    @Column(name = "PRODUCT_ID")
    protected String productId;

    @Column(name = "QUANTITY")
    protected Integer quantity;

    @Column(name = "PRICE")
    protected BigDecimal price;

   
    @Column(name = "SHIPPED_DATE")
    protected LocalDate shippedDate;


    @Column(name = "PAYMENT_METHOD")
    protected String paymentMethod;

   

    @Column(name = "ORDER_DATES")
    protected String orderDates;

 


    @Column(name = "CUSTOMER_ADDRESS")
    protected String customerAddress;

    @Column(name = "PROMO_CODE")
    protected String promoCode;

    @Column(name = "APPLIED_COUPON_AMOUNT")
    protected BigDecimal appliedCouponAmount;

    @Column(name = "GIFT_CARD_ID")
    protected String giftCardId;

    @Column(name = "GIFT_CARD_AMOUNT")
    protected BigDecimal giftCardAmount;

    @Column(name = "SUB_TOTAL")
    protected BigDecimal subTotal;

  
    @Column(name = "CREATED_AT")
    protected Instant createdAt;

 
    @Column(name = "UPDATED_AT")
    protected Instant updatedAt;

    @Column(name = "CUSTOMER_ID")
    protected String customerId;


    @Column(name = "VENDOR_ID")
    protected String vendorId;

   


    @Column(name = "ORDER_DATE")
    protected LocalDate orderDate;

    @Column(name = "IS_PROGRESSED")
    protected Boolean isProgressed=false;

    @Column(name = "IS_SHIPPED")
    protected Boolean isShipped=false;
    
   
    
    /*
     * 
    @Enumerated(EnumType.STRING)
    @Column(name = "ORDER_STATUS")
    protected OrderStatus orderStatus;
    public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
   
    @Enumerated(EnumType.STRING)
    @Column(name = "FULL_ORDER_STATUS")
    protected FullOrderStatus fullOrderStatus;
    @Enumerated(EnumType.STRING)
    @Column(name = "PAYMENT_STATUS")
    protected PaymentStatus paymentStatus;
    
    public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public FullOrderStatus getFullOrderStatus() {
		return fullOrderStatus;
	}

	public void setFullOrderStatus(FullOrderStatus fullOrderStatus) {
		this.fullOrderStatus = fullOrderStatus;
	}
	
	
	
	*/

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getVendorCompanyId() {
		return vendorCompanyId;
	}

	public void setVendorCompanyId(String vendorCompanyId) {
		this.vendorCompanyId = vendorCompanyId;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getShipmentFee() {
		return shipmentFee;
	}

	public void setShipmentFee(BigDecimal shipmentFee) {
		this.shipmentFee = shipmentFee;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public LocalDate getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(LocalDate deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public String getTrackingUrl() {
		return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getResetToken() {
		return resetToken;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public Instant getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Instant timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Integer getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(Integer orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public LocalDate getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(LocalDate shippedDate) {
		this.shippedDate = shippedDate;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}



	public String getOrderDates() {
		return orderDates;
	}

	public void setOrderDates(String orderDates) {
		this.orderDates = orderDates;
	}

	

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public BigDecimal getAppliedCouponAmount() {
		return appliedCouponAmount;
	}

	public void setAppliedCouponAmount(BigDecimal appliedCouponAmount) {
		this.appliedCouponAmount = appliedCouponAmount;
	}

	public String getGiftCardId() {
		return giftCardId;
	}

	public void setGiftCardId(String giftCardId) {
		this.giftCardId = giftCardId;
	}

	public BigDecimal getGiftCardAmount() {
		return giftCardAmount;
	}

	public void setGiftCardAmount(BigDecimal giftCardAmount) {
		this.giftCardAmount = giftCardAmount;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public Instant getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}

	public Instant getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Instant updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public Boolean getIsProgressed() {
		return isProgressed;
	}

	public void setIsProgressed(Boolean isProgressed) {
		this.isProgressed = isProgressed;
	}

	public Boolean getIsShipped() {
		return isShipped;
	}

	public void setIsShipped(Boolean isShipped) {
		this.isShipped = isShipped;
	}


 
}
