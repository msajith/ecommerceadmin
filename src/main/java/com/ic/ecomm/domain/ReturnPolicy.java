package com.ic.ecomm.domain;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

/**
 * A ReturnPolicy.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_RETURN_POLICY")
public class ReturnPolicy implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;
    

    public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@NotNull
    @Column(name = "NAME", nullable = false, unique = true)
    protected String name;

    @Column(name = "RETURNABLE")
    protected Boolean returnable;

    @NotNull
    @Column(name = "DESCRIPTION", nullable = false)
    protected String description;

    @Column(name = "POLICY_ID")
    protected String policyId;

    @Column(name = "CLIENT_ID")
    protected String clientId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getReturnable() {
		return returnable;
	}

	public void setReturnable(Boolean returnable) {
		this.returnable = returnable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
    

}
