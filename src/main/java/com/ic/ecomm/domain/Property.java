package com.ic.ecomm.domain;
import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

/**
 * A Property.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_PROPERTIES")
public class Property implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @NotNull
    @Column(name = "PROPERTY_NAME", nullable = false)
    protected String propertyName;

    @Column(name = "FILTER_")
    protected Boolean filter;

    @Column(name = "IMAGE")
    protected String image;

    @Column(name = "VARIANT")
    protected Boolean variant;

    @Column(name = "PROPERTY_COUNT")
    protected Integer propertyCount;

    @NotNull
    @Column(name = "PROPERTY_VALUE", nullable = false)
    protected String propertyValue;



    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTS_ID")
    private Product product;



	public UUID getId() {
		return id;
	}



	public void setId(UUID id) {
		this.id = id;
	}



	public String getPropertyName() {
		return propertyName;
	}



	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}



	public Boolean getFilter() {
		return filter;
	}



	public void setFilter(Boolean filter) {
		this.filter = filter;
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



	public Boolean getVariant() {
		return variant;
	}



	public void setVariant(Boolean variant) {
		this.variant = variant;
	}



	public Integer getPropertyCount() {
		return propertyCount;
	}



	public void setPropertyCount(Integer propertyCount) {
		this.propertyCount = propertyCount;
	}



	public String getPropertyValue() {
		return propertyValue;
	}



	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}



	public Product getProduct() {
		return product;
	}



	public void setProduct(Product product) {
		this.product = product;
	}
    
    

  
}
