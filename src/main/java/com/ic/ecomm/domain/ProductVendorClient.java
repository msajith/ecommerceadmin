package com.ic.ecomm.domain;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.UUID;

/**
 * A ProductVendorClient.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_PRODUCT_VENDOR_CLIENTS")
public class ProductVendorClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "CLIENT_ID")
    protected String clientId;

    @Column(name = "VENDOR")
    protected String vendor;

    @Column(name = "PRODUCT_ID")
    protected String productId;

    @Column(name = "PRODUCT_CATEGORY_ID")
    protected String productCategoryId;

    @Column(name = "PRODUCT_VENDOR_ID")
    protected String productVendorId;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public String getProductVendorId() {
		return productVendorId;
	}

	public void setProductVendorId(String productVendorId) {
		this.productVendorId = productVendorId;
	}



   
}
