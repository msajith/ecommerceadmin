package com.ic.ecomm.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    DELIVERD, SHIPPED, PROCESSING,NONE;
    public static OrderStatus from(String text) {
        if (text == null || "null".equalsIgnoreCase(text)) {
          return NONE;
        } else {
        	return valueOf(text.toUpperCase());
        }
      }
}
