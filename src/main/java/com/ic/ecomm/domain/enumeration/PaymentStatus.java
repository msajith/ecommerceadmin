package com.ic.ecomm.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    SUCCESS, PENDING, FAILED,None;
    public static PaymentStatus from(String text) {
        if (text == null || "null".equalsIgnoreCase(text)) {
          return None;
        } else {
        	return valueOf(text.toUpperCase());
        }
      }
}
