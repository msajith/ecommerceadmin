package com.ic.ecomm.domain.enumeration;

/**
 * The FullOrderStatus enumeration.
 */
public enum FullOrderStatus {
    Delivered, Shipped, Processing,None;
    public static FullOrderStatus from(String text) {
        if (text == null || "null".equalsIgnoreCase(text)) {
          return None;
        } else {
        	return valueOf(text.toUpperCase());
        }
      }
}
