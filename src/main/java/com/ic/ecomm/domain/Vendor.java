package com.ic.ecomm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

/**
 * A Vendor.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_VENDOR", uniqueConstraints = {
	    @UniqueConstraint(name = "IDX_ECOMMERCEADMIN_VENDOR_UNQ", columnNames = {"CLIENT_ID", "VENDOR_ID"})
	})

public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;
    
    @NotNull
    @Column(name = "CLIENT_ID", nullable = false)
    protected String clientId;

    @Column(name = "CELL_ID")
    protected String cellId;

    
    @Column(name = "ADDRESS_LINE1")
    protected String addressLine1;

    
    @Column(name = "ADDRESS_LINE2")
    protected String addressLine2;

    @Column(name = "ID_PROOF")
    protected String idProof;

    @Column(name = "DOCUMENT")
    protected String document;

    @Column(name = "AGENT_ID")
    protected String agentId;

    @Column(name = "MAX_DUE_AMOUNT")
    protected BigDecimal maxDueAmount;

    @NotNull
    @Column(name = "VENDOR_EMAIL", nullable = false)
    protected String vendorEmail;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIMESTAMP_")
    protected Date timestamp;

    @Column(name = "DELIVERY_COUNT")
    protected Integer deliveryCount;

    @Column(name = "ADDRESS_COUNT")
    protected Integer addressCount;

    
    @OneToMany(mappedBy = "vendor")
    protected List<Address> address;

 


    @Column(name = "LATITUDE")
    protected String latitude;

    @Column(name = "LONGITUDE")
    protected String longitude;


    @Column(name = "VENDOR_CLASS")
    protected String vendorClass;


    @NotNull
    @Column(name = "VENDOR_CONTACT", nullable = false)
    protected String vendorContact;

    @NotNull
    @Column(name = "VENDOR_ID", nullable = false)
    protected String vendorId;

    @NotNull
    @Column(name = "VENDOR_NAME", nullable = false, unique = true)
    protected String vendorName;

    @Column(name = "VENDOR_COMPANY_NAME")
    protected String vendorCompanyName;

    @Column(name = "VENDOR_COMPANY_ID")
    protected String vendorCompanyId;

    @Column(name = "COUNTRY")
    protected String country;

    @Column(name = "STATE")
    protected String state;

    @Column(name = "DISTRICT")
    protected String district;


   
    public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getClientId() {
        return clientId;
    }

    public Vendor clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCellId() {
        return cellId;
    }

    public Vendor cellId(String cellId) {
        this.cellId = cellId;
        return this;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public Vendor addressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
        return this;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public Vendor addressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
        return this;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getIdProof() {
        return idProof;
    }

    public Vendor idProof(String idProof) {
        this.idProof = idProof;
        return this;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getDocument() {
        return document;
    }

    public Vendor document(String document) {
        this.document = document;
        return this;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getAgentId() {
        return agentId;
    }

    public Vendor agentId(String agentId) {
        this.agentId = agentId;
        return this;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public BigDecimal getMaxDueAmount() {
        return maxDueAmount;
    }

    public Vendor maxDueAmount(BigDecimal maxDueAmount) {
        this.maxDueAmount = maxDueAmount;
        return this;
    }

    public void setMaxDueAmount(BigDecimal maxDueAmount) {
        this.maxDueAmount = maxDueAmount;
    }

    public String getVendorEmail() {
        return vendorEmail;
    }

    public Vendor vendorEmail(String vendorEmail) {
        this.vendorEmail = vendorEmail;
        return this;
    }

    public void setVendorEmail(String vendorEmail) {
        this.vendorEmail = vendorEmail;
    }

    public Integer getDeliveryCount() {
        return deliveryCount;
    }

    public Vendor deliveryCount(Integer deliveryCount) {
        this.deliveryCount = deliveryCount;
        return this;
    }

    public void setDeliveryCount(Integer deliveryCount) {
        this.deliveryCount = deliveryCount;
    }

    public Integer getAddressCount() {
        return addressCount;
    }

    public Vendor addressCount(Integer addressCount) {
        this.addressCount = addressCount;
        return this;
    }

    public void setAddressCount(Integer addressCount) {
        this.addressCount = addressCount;
    }

    public String getLatitude() {
        return latitude;
    }

    public Vendor latitude(String latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Vendor longitude(String longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getVendorClass() {
        return vendorClass;
    }

    public Vendor vendorClass(String vendorClass) {
        this.vendorClass = vendorClass;
        return this;
    }

    public void setVendorClass(String vendorClass) {
        this.vendorClass = vendorClass;
    }

    public String getVendorContact() {
        return vendorContact;
    }

    public Vendor vendorContact(String vendorContact) {
        this.vendorContact = vendorContact;
        return this;
    }

    public void setVendorContact(String vendorContact) {
        this.vendorContact = vendorContact;
    }

    public String getVendorId() {
        return vendorId;
    }

    public Vendor vendorId(String vendorId) {
        this.vendorId = vendorId;
        return this;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public Vendor vendorName(String vendorName) {
        this.vendorName = vendorName;
        return this;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorCompanyName() {
        return vendorCompanyName;
    }

    public Vendor vendorCompanyName(String vendorCompanyName) {
        this.vendorCompanyName = vendorCompanyName;
        return this;
    }

    public void setVendorCompanyName(String vendorCompanyName) {
        this.vendorCompanyName = vendorCompanyName;
    }

    public String getVendorCompanyId() {
        return vendorCompanyId;
    }

    public Vendor vendorCompanyId(String vendorCompanyId) {
        this.vendorCompanyId = vendorCompanyId;
        return this;
    }

    public void setVendorCompanyId(String vendorCompanyId) {
        this.vendorCompanyId = vendorCompanyId;
    }

    public String getCountry() {
        return country;
    }

    public Vendor country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public Vendor state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public Vendor district(String district) {
        this.district = district;
        return this;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

 


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vendor)) {
            return false;
        }
        return id != null && id.equals(((Vendor) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Vendor{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", cellId='" + getCellId() + "'" +
            ", addressLine1='" + getAddressLine1() + "'" +
            ", addressLine2='" + getAddressLine2() + "'" +
            ", idProof='" + getIdProof() + "'" +
            ", document='" + getDocument() + "'" +
            ", agentId='" + getAgentId() + "'" +
            ", maxDueAmount=" + getMaxDueAmount() +
            ", vendorEmail='" + getVendorEmail() + "'" +
            ", deliveryCount=" + getDeliveryCount() +
            ", addressCount=" + getAddressCount() +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", vendorClass='" + getVendorClass() + "'" +
            ", vendorContact='" + getVendorContact() + "'" +
            ", vendorId='" + getVendorId() + "'" +
            ", vendorName='" + getVendorName() + "'" +
            ", vendorCompanyName='" + getVendorCompanyName() + "'" +
            ", vendorCompanyId='" + getVendorCompanyId() + "'" +
            ", country='" + getCountry() + "'" +
            ", state='" + getState() + "'" +
            ", district='" + getDistrict() + "'" +
            "}";
    }
}
