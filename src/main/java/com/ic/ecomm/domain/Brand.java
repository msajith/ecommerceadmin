package com.ic.ecomm.domain;
import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import io.swagger.annotations.ApiModel;


@Entity
@Table(name = "ECOMMERCEADMIN_BRAND")
public class Brand implements Serializable {

    //private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;
    
    @NotNull
    @Column(name = "BRAND_NAME")
    protected String name;

    @NotNull
    @Column(name = "BRAND_ID")
    protected String brandId;

   
    @Column(name = "BRAND_DESCRIPTION")
    protected String brandDescription;

    @Column(name = "BRAND_URL")
    protected String brandUrl;

    @Column(name = "BRAND_LOGO")
    protected String brandLogo;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandDescription() {
		return brandDescription;
	}

	public void setBrandDescription(String brandDescription) {
		this.brandDescription = brandDescription;
	}

	public String getBrandUrl() {
		return brandUrl;
	}

	public void setBrandUrl(String brandUrl) {
		this.brandUrl = brandUrl;
	}

	public String getBrandLogo() {
		return brandLogo;
	}

	public void setBrandLogo(String brandLogo) {
		this.brandLogo = brandLogo;
	}

	
	
    

}
