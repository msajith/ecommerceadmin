package com.ic.ecomm.domain;
import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Description.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_DESCRIPTION")
public class Description implements Serializable {

    private static final long serialVersionUID = 1L;

    
    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;
    
    @NotNull
    @Column(name = "DESCRIPTION", nullable = false)
    protected String description;


    @Column(name = "DESCRIPTION_COUNT")
    protected Integer descriptionCount;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTS_ID")
    private Product product;


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getDescriptionCount() {
		return descriptionCount;
	}


	public void setDescriptionCount(Integer descriptionCount) {
		this.descriptionCount = descriptionCount;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public UUID getId() {
		return id;
	}


	public void setId(UUID id) {
		this.id = id;
	}
	
  
}
