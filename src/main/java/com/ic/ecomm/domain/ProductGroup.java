package com.ic.ecomm.domain;
import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

/**
 * A ProductGroup.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_PRODUCT_GROUPING")
public class ProductGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @NotNull
    @Column(name = "PRODUCT_GROUP", nullable = false, unique = true)
    protected String productGroup;


    @Column(name = "CLIENT_ID")
    protected String clientId;

    @Column(name = "PRODUCT_GROUP_ID")
    protected String productGroupId;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID")
    private Product products;


	public UUID getId() {
		return id;
	}


	public void setId(UUID id) {
		this.id = id;
	}


	public String getProductGroup() {
		return productGroup;
	}


	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public String getProductGroupId() {
		return productGroupId;
	}


	public void setProductGroupId(String productGroupId) {
		this.productGroupId = productGroupId;
	}


	public Product getProducts() {
		return products;
	}


	public void setProducts(Product products) {
		this.products = products;
	}

   
}
