package com.ic.ecomm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

/**
 * A Inventory.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_INVENTORY", uniqueConstraints = {
        @UniqueConstraint(name = "IDX_ECOMMERCEADMIN_INVENTORY_UNQ", columnNames = {"PRODUCT_NAME", "VENDOR_ID"})
})

public class Inventory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @NotNull
    @Column(name = "PRODUCT_NAME", nullable = false)
    protected String productName;

    @Column(name = "VENDOR_COMPANY_ID")
    protected String vendorCompanyId;

    @Column(name = "VENDOR_NAME")
    protected String vendorName;

    @Column(name = "CLIENT_ID")
    protected String clientId;

    @Column(name = "VENDOR_ID")
    protected String vendorId;

    
    @Column(name = "PRODUCT_ID")
    protected String productId;

    @Column(name = "STATUS")
    protected String status;

    @Column(name = "UNIT_PRICE")
    protected BigDecimal unitPrice;

    @NotNull
    @Column(name = "COSTPRICE", nullable = false)
    protected BigDecimal costprice;

    @NotNull
    @Column(name = "MAX_QUANTITY", nullable = false)
    protected Integer maxQuantity;

    @NotNull
    @Column(name = "AVAILABLESTOCK", nullable = false)
    protected Integer availablestock;

    @Column(name = "SALES_QUANTITY")
    protected Integer salesQuantity;

    public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Column(name = "PRICE_TAG")
    protected String priceTag;

    @Column(name = "PRODUCT_VENDOR_ID")
    protected String productVendorId;

    @Column(name = "PRODUCT_CATEGORY_ID")
    protected String productCategoryId;


    

    public String getProductName() {
        return productName;
    }

    public Inventory productName(String productName) {
        this.productName = productName;
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVendorCompanyId() {
        return vendorCompanyId;
    }

    public Inventory vendorCompanyId(String vendorCompanyId) {
        this.vendorCompanyId = vendorCompanyId;
        return this;
    }

    public void setVendorCompanyId(String vendorCompanyId) {
        this.vendorCompanyId = vendorCompanyId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public Inventory vendorName(String vendorName) {
        this.vendorName = vendorName;
        return this;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getClientId() {
        return clientId;
    }

    public Inventory clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public Inventory vendorId(String vendorId) {
        this.vendorId = vendorId;
        return this;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getProductId() {
        return productId;
    }

    public Inventory productId(String productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStatus() {
        return status;
    }

    public Inventory status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public Inventory unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getCostprice() {
        return costprice;
    }

    public Inventory costprice(BigDecimal costprice) {
        this.costprice = costprice;
        return this;
    }

    public void setCostprice(BigDecimal costprice) {
        this.costprice = costprice;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public Inventory maxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
        return this;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public Integer getAvailablestock() {
        return availablestock;
    }

    public Inventory availablestock(Integer availablestock) {
        this.availablestock = availablestock;
        return this;
    }

    public void setAvailablestock(Integer availablestock) {
        this.availablestock = availablestock;
    }

    public Integer getSalesQuantity() {
        return salesQuantity;
    }

    public Inventory salesQuantity(Integer salesQuantity) {
        this.salesQuantity = salesQuantity;
        return this;
    }

    public void setSalesQuantity(Integer salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    public String getPriceTag() {
        return priceTag;
    }

    public Inventory priceTag(String priceTag) {
        this.priceTag = priceTag;
        return this;
    }

    public void setPriceTag(String priceTag) {
        this.priceTag = priceTag;
    }

    public String getProductVendorId() {
        return productVendorId;
    }

    public Inventory productVendorId(String productVendorId) {
        this.productVendorId = productVendorId;
        return this;
    }

    public void setProductVendorId(String productVendorId) {
        this.productVendorId = productVendorId;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public Inventory productCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
        return this;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Inventory)) {
            return false;
        }
        return id != null && id.equals(((Inventory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

   
}
