package com.ic.ecomm.domain;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

/**
 * A Product.
 */
@Entity
@Table(name = "ECOMMERCEADMIN_PRODUCTS", uniqueConstraints = {
        @UniqueConstraint(name = "IDX_ECOMMERCEADMIN_PRODUCTS_UNQ", columnNames = {"CLIENT_ID", "PRODUCT_NAME"})
})

public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "CLIENT_ID")
    protected String clientId;


    @Column(name = "VENDOR_COMPANY_ID")
    protected String vendorCompanyId;

  
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RETURN_POLICY_ID")
    protected ReturnPolicy returnPolicy;

    @Column(name = "RETURN_DAYS_LIMIT")
    protected Integer returnDaysLimit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_GROUP_ID")
    protected ProductGroup productGroup;



    @Column(name = "PROPERTY_COUNT")
    protected Integer propertyCount;

    @Column(name = "DESCRIPTION_COUNT")
    protected Integer descriptionCount;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CATEGORY_ID")
    protected Category category;

    @Column(name = "PRODUCT_CATEGORY_ID")
    protected String productCategoryId;


    @Column(name = "VENDOR_ID")
    protected String vendorId;



    @Column(name = "TAG_CODE")
    protected String tagCode;

    @Column(name = "MODEL_CODE")
    protected String modelCode;

    @NotNull
    @Column(name = "PRODUCT_NAME", nullable = false, unique = true)
    protected String productName;

    @Column(name = "PRODUCT_ID")
    protected String productId;


    @ManyToOne
    @JoinColumn(name = "brand",referencedColumnName="brand_id")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Brand brand;
    

    @Column(name = "DESCRIPTION")
    protected String description;

    @Column(name = "WEIGHT")
    protected Double weight;

    @Column(name = "UNIT")
    protected String unit;

   
    @Column(name = "SHORT_DESCRIPTION")
    protected String shortDescription;


    @Column(name = "IMAGE1")
    protected String image1;

    @Column(name = "IMAGE2")
    protected String image2;

    @Column(name = "IMAGE3")
    protected String image3;

    @Column(name = "IMAGE4")
    protected String image4;

    @Column(name = "BACKGROUND_IMAGE")
    protected String backgroundImage;


    @Column(name = "STATUS")
    protected String status;



    @OneToMany(mappedBy = "product")
    protected List<Description> highlights;

  
    @OneToMany(mappedBy = "product")
    protected List<Property> properties;


	public UUID getId() {
		return id;
	}


	public void setId(UUID id) {
		this.id = id;
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public String getVendorCompanyId() {
		return vendorCompanyId;
	}


	public void setVendorCompanyId(String vendorCompanyId) {
		this.vendorCompanyId = vendorCompanyId;
	}


	public ReturnPolicy getReturnPolicy() {
		return returnPolicy;
	}


	public void setReturnPolicy(ReturnPolicy returnPolicy) {
		this.returnPolicy = returnPolicy;
	}


	public Integer getReturnDaysLimit() {
		return returnDaysLimit;
	}


	public void setReturnDaysLimit(Integer returnDaysLimit) {
		this.returnDaysLimit = returnDaysLimit;
	}


	public ProductGroup getProductGroup() {
		return productGroup;
	}


	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}


	public Integer getPropertyCount() {
		return propertyCount;
	}


	public void setPropertyCount(Integer propertyCount) {
		this.propertyCount = propertyCount;
	}


	public Integer getDescriptionCount() {
		return descriptionCount;
	}


	public void setDescriptionCount(Integer descriptionCount) {
		this.descriptionCount = descriptionCount;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}


	public String getProductCategoryId() {
		return productCategoryId;
	}


	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}


	public String getVendorId() {
		return vendorId;
	}


	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}


	public String getTagCode() {
		return tagCode;
	}


	public void setTagCode(String tagCode) {
		this.tagCode = tagCode;
	}


	public String getModelCode() {
		return modelCode;
	}


	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductId() {
		return productId;
	}


	public void setProductId(String productId) {
		this.productId = productId;
	}


	public Brand getBrand() {
		return brand;
	}


	public void setBrand(Brand brand) {
		this.brand = brand;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Double getWeight() {
		return weight;
	}


	public void setWeight(Double weight) {
		this.weight = weight;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public String getShortDescription() {
		return shortDescription;
	}


	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}


	public String getImage1() {
		return image1;
	}


	public void setImage1(String image1) {
		this.image1 = image1;
	}


	public String getImage2() {
		return image2;
	}


	public void setImage2(String image2) {
		this.image2 = image2;
	}


	public String getImage3() {
		return image3;
	}


	public void setImage3(String image3) {
		this.image3 = image3;
	}


	public String getImage4() {
		return image4;
	}


	public void setImage4(String image4) {
		this.image4 = image4;
	}


	public String getBackgroundImage() {
		return backgroundImage;
	}


	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public List<Description> getHighlights() {
		return highlights;
	}


	public void setHighlights(List<Description> highlights) {
		this.highlights = highlights;
	}


	public List<Property> getProperties() {
		return properties;
	}


	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

  


   
   
}
