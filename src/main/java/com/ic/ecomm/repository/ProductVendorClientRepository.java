package com.ic.ecomm.repository;

import com.ic.ecomm.domain.ProductVendorClient;

import java.util.UUID;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProductVendorClient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductVendorClientRepository extends JpaRepository<ProductVendorClient, UUID> {

}
