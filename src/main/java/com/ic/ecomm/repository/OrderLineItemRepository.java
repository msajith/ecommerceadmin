package com.ic.ecomm.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.domain.OrderLineItem;


/**
 * Spring Data  repository for the OrderLineItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderLineItemRepository extends JpaRepository<OrderLineItem, UUID> {
	
	@Query(value = "select * from ecommerceadmin_order_details where client_id=?1 and vendor_company_id=?2 and vendor_id=?3", nativeQuery = true)
	 List<OrderLineItem> findByMerchantAndStore(String tenantId, String merchantId, String storeId);
	
	@Query(value = "select * from ecommerceadmin_order_details where client_id=?1 and vendor_company_id=?2 ", nativeQuery = true)
	 List<OrderLineItem> findByMerchant(String tenantId, String merchantId);
	
	@Query(value = "select * from ecommerceadmin_order_details where client_id=?1 and vendor_id=?2", nativeQuery = true)
	 List<OrderLineItem> findByStore(String tenantId, String storeId);
	

}
