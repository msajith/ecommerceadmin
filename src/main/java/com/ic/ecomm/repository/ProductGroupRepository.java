package com.ic.ecomm.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.domain.ProductGroup;


/**
 * Spring Data  repository for the ProductGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductGroupRepository extends JpaRepository<ProductGroup, UUID> {

}
