package com.ic.ecomm.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.domain.ReturnPolicy;


/**
 * Spring Data  repository for the ReturnPolicy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReturnPolicyRepository extends JpaRepository<ReturnPolicy, UUID> {

}
