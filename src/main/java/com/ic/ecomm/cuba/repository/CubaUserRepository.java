package com.ic.ecomm.cuba.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.cuba.domain.CubaUser;
import com.ic.ecomm.domain.User;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface CubaUserRepository extends JpaRepository<CubaUser, UUID> {

 
    Optional<CubaUser> findOneByLogin(String login);
    
    @EntityGraph(attributePaths = "authorities")
    Optional<CubaUser> findOneWithAuthoritiesByLogin(String login);

}
