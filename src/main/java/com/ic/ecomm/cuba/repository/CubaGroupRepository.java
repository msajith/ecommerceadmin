package com.ic.ecomm.cuba.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.cuba.domain.CubaGroup;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface CubaGroupRepository extends JpaRepository<CubaGroup, UUID> {

 
   
}
