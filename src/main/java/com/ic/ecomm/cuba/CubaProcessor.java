package com.ic.ecomm.cuba;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.UUID;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.haulmont.cuba.core.sys.encryption.EncryptionModule;
import com.haulmont.cuba.core.sys.encryption.Sha1EncryptionModule;

public class CubaProcessor {

	
	private static String authURL=ResourceBundle.getBundle("app").getString("cuba.url.auth");
	private static String createUserUrl=ResourceBundle.getBundle("app").getString("cuba.url.create-user");
	private static String getGroupUrl=ResourceBundle.getBundle("app").getString("cuba.url.get-group");
	private static String getTenantGroupUrl=ResourceBundle.getBundle("app").getString("cuba.url.get-tenant-group");
	private static String getRoleUrl=ResourceBundle.getBundle("app").getString("cuba.url.get-role");
	private static String createVendorRequestUrl=ResourceBundle.getBundle("app").getString("cuba.url.create-vendor-request");
	private static String createGroupUrl=ResourceBundle.getBundle("app").getString("cuba.url.create-group");
	private static String createVendorCompanyUrl=ResourceBundle.getBundle("app").getString("cuba.url.create-vendor-company");
	
	
	private static String cubaApiUserName=ResourceBundle.getBundle("app").getString("cuba.url.username");
	private static String cubaApiUserPassword=ResourceBundle.getBundle("app").getString("cuba.url.password");
	
	private static String clientId=ResourceBundle.getBundle("app").getString("cuba.url.clientid");
	private static String secret=ResourceBundle.getBundle("app").getString("cuba.url.secret");
	
	//private static String merchantGroup=ResourceBundle.getBundle("app").getString("cuba.merchant.group");	
	private static String merchantRole=ResourceBundle.getBundle("app").getString("cuba.merchant.role");
	
	private static String tenantId=ResourceBundle.getBundle("app").getString("tenant_id");
	
	private static String cubaApiTenantUserName=ResourceBundle.getBundle("app").getString("cuba.url.tenent.username");
	private static String cubaApiTenantUserPassword=ResourceBundle.getBundle("app").getString("cuba.url.tenent.password");
	
	
	
	
	
	private static EncryptionModule encryptionModule = new Sha1EncryptionModule();
	 
	
	final static String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";

	final static java.util.Random rand = new java.util.Random();

	// consider using a Map<String,Boolean> to say whether the identifier is being used or not 
	final static Set<String> identifiers = new HashSet<String>();
	
	public static String randomIdentifier() {
	    StringBuilder builder = new StringBuilder();
	    while(builder.toString().length() == 0) {
	        int length = rand.nextInt(5)+5;
	        for(int i = 0; i < length; i++) {
	            builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
	        }
	        if(identifiers.contains(builder.toString())) {
	            builder = new StringBuilder();
	        }
	    }
	   
	    return builder.toString();
	}
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(getAuthToken());

		//System.out.println(createNewGroup("LASTGRPTEST"));
	//System.out.println(createUser("ZZZZ_YYYYYZZZ","mohammed","msajith@gmail.com","999999999"));

	}
	
	static HttpHeaders createHeaders(String username, String password){
		   return new HttpHeaders() {{
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.getEncoder().encode( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		      }};
		}

	public static String  getAuthToken() {
		String result =null;

		RestTemplate restTemplate = new RestTemplate();

		

		HttpHeaders headers = createHeaders(clientId,secret);//new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("username", cubaApiUserName);
		map.add("password", cubaApiUserPassword);
		map.add("grant_type", "password");
		
		HttpEntity<MultiValueMap<String, String>> requestEntity= new HttpEntity<>(map, headers);
		

		//HttpEntity entity = new HttpEntity(jObject.toString(), headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(authURL,HttpMethod.POST, requestEntity, String.class);
			JSONObject json = new JSONObject(response.getBody());
			result = json.get("access_token").toString();
			//System.out.println(json.get("access_token"));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	public static String  getTenantAuthToken() {
		String result =null;

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = createHeaders(clientId,secret);//new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("username", cubaApiTenantUserName);
		map.add("password", cubaApiTenantUserPassword);
		map.add("grant_type", "password");
		
		HttpEntity<MultiValueMap<String, String>> requestEntity= new HttpEntity<>(map, headers);
		

		//HttpEntity entity = new HttpEntity(jObject.toString(), headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(authURL,HttpMethod.POST, requestEntity, String.class);
			JSONObject json = new JSONObject(response.getBody());
			result = json.get("access_token").toString();
			//System.out.println(json.get("access_token"));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	
	public static JSONObject createUser(String firstName,String lastName,String email,String phone) {
		JSONObject result =null;

		RestTemplate restTemplate = new RestTemplate();
		
		String userName=randomIdentifier();
		String password=randomIdentifier();
		System.out.println("Username : "+ userName);
		System.out.println("Password : "+ password);
		
		createVendorReqquest(firstName,lastName,email,phone,userName,password);
		
		JSONObject userObj = new JSONObject();
		
		userObj.put("firstName", firstName);
		userObj.put("lastName", lastName);
		userObj.put("tenantId", tenantId);
		
		userObj.put("login", userName);
		userObj.put("password",password);
		userObj.put("confirmPassword",password);
		userObj.put("_instanceName","userName");//randomIdentifier());
		userObj.put("name", firstName);
		JSONObject vendorCompany = createVendorCompany(firstName, "", email, phone);
		String groupId = vendorCompany.getString("groupId");
		JSONObject accessGroup = getGroup(groupId);
		//createNewGroup(firstName);
		userObj.put("group", accessGroup);
	
		
		
		JSONObject userRole = new JSONObject();
		userRole.put("role", getRole(merchantRole));
		
		JSONObject[] userRoles = new JSONObject[1];
		userRoles[0] = userRole;
		userObj.put("userRoles", userRoles);
		
		
		//userObje.put
	
		//System.out.println(userObj);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity entity = new HttpEntity(userObj.toString(), headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(createUserUrl,HttpMethod.POST, entity, String.class);
			JSONObject json = new JSONObject(response.getBody());
			String userId = json.get("id").toString();
			
			//Update the password
			String claculatedHash = encryptionModule.getPasswordHash(UUID.fromString(userId), password.toString());
			json.put("password", claculatedHash);
			entity = new HttpEntity(json.toString(), headers);
					
			ResponseEntity<String> updateResponse = restTemplate.exchange(createUserUrl+"/"+userId,HttpMethod.PUT, entity, String.class);
			json = new JSONObject(updateResponse.getBody());
			
			result =userObj;//updateResponse.getBody();
			
			//Update the Roles 
			
			
			System.out.println(json);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	public static String createVendorReqquest(String firstName,String lastName,String email,String phone,String userName,String password) {
		String result =null;

		RestTemplate restTemplate = new RestTemplate();
		
	
		JSONObject vendoRrequest = new JSONObject();
		
		vendoRrequest.put("client_id",tenantId);
		
		vendoRrequest.put("vendorFirstname", firstName);
		vendoRrequest.put("vendorLastname", lastName);
		
		vendoRrequest.put("vendorMailId",email);
		vendoRrequest.put("phoneNumber",phone);
		
		vendoRrequest.put("username",userName);
		vendoRrequest.put("password",password);//randomIdentifier());
		
		
	
		//System.out.println(vendoRrequest);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity entity = new HttpEntity(vendoRrequest.toString(), headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(createVendorRequestUrl,HttpMethod.POST, entity, String.class);
			JSONObject json = new JSONObject(response.getBody());
			
			System.out.println(json);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	public static JSONObject createVendorCompany(String merchantName,String groupId,String email,String phone) {
		JSONObject result =null;

		RestTemplate restTemplate = new RestTemplate();
		
	
		JSONObject vendorcompany = new JSONObject();
		
		vendorcompany.put("client_id",tenantId);
		
		vendorcompany.put("companyName", merchantName);
		vendorcompany.put("groupId", groupId);
		
		vendorcompany.put("email",email);
		vendorcompany.put("phoneNumber",phone);
		
		vendorcompany.put("vendorCompanyId",merchantName);
		
		
		
	
		System.out.println(vendorcompany);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getTenantAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		System.out.println();
		HttpEntity entity = new HttpEntity(vendorcompany.toString(), headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(createVendorCompanyUrl,HttpMethod.POST, entity, String.class);
			JSONObject json = new JSONObject(response.getBody());
			
			System.out.println(json);
			result = json;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	
	public static JSONObject createNewGroup(String name) {
		JSONObject result =null;

		RestTemplate restTemplate = new RestTemplate();
		
	
		JSONObject groupObj = new JSONObject();
		
		groupObj.put("name",name);
		groupObj.put("parent",getTenantGroup());
		
		JSONObject[] sessionAttributes = new JSONObject[4];
		
		JSONObject sessionAttribute1 = new JSONObject();
		sessionAttribute1.put("name","client_session");
		sessionAttribute1.put("datatype","string");
		sessionAttribute1.put("stringValue","women-power");
		sessionAttribute1.put( "datatypeCaption", "String");		
		sessionAttributes[0]=sessionAttribute1;
		
		JSONObject sessionAttribute2 = new JSONObject();
		sessionAttribute2.put("name","vendor_company_session");
		sessionAttribute2.put("datatype","string");
		sessionAttribute2.put("stringValue",name);
		sessionAttribute2.put( "datatypeCaption", "String");		
		sessionAttributes[1]=sessionAttribute2;
		
		
		JSONObject sessionAttribute3 = new JSONObject();
		sessionAttribute3.put("name","adminType");
		sessionAttribute3.put("datatype","string");
		sessionAttribute3.put("stringValue","VendorCompanyAdmin");
		sessionAttribute3.put( "datatypeCaption", "String");		
		sessionAttributes[2]=sessionAttribute3;
		
		JSONObject sessionAttribute4 = new JSONObject();
		sessionAttribute4.put("name","identifier_id");
		sessionAttribute4.put("datatype","string");
		sessionAttribute4.put("stringValue",name+"-id");
		sessionAttribute4.put( "datatypeCaption", "String");		
		sessionAttributes[3]=sessionAttribute4;
		
		
		
		
		groupObj.put("sessionAttributes", sessionAttributes);
		
		
		JSONObject[] constraints = new JSONObject[8];
		
		JSONObject constraint0 = new JSONObject();
		constraint0.put("checkType","DATABASE");
		constraint0.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint0.put("isActive",true);
		constraint0.put( "entityName", "ecommerceadmin$Vendor");
		constraint0.put( "operationType", "READ");		
		constraints[0]=constraint0;
		
		JSONObject constraint1 = new JSONObject();
		constraint1.put("checkType","DATABASE");
		constraint1.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint1.put("isActive",true);
		constraint1.put( "entityName", "ecommerceadmin$Product");
		constraint1.put( "operationType", "READ");		
		constraints[1]=constraint1;
		
		
		JSONObject constraint2 = new JSONObject();
		constraint2.put("checkType","DATABASE");
		constraint2.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint2.put("isActive",true);
		constraint2.put( "entityName", "ecommerceadmin$Inventory");
		constraint2.put( "operationType", "READ");		
		constraints[2]=constraint2;
		
		JSONObject constraint3 = new JSONObject();
		constraint3.put("checkType","DATABASE");
		constraint3.put("whereClause"," {E}.description is null or {E}.description not like 'hide' ");
		constraint3.put("isActive",true);
		constraint3.put( "entityName", "sec$Role");
		constraint3.put( "operationType", "READ");		
		constraints[3]=constraint3;
		
		JSONObject constraint4 = new JSONObject();
		constraint4.put("checkType","DATABASE");
		constraint4.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint4.put("isActive",true);
		constraint4.put( "entityName", "ecommerceadmin$Product");
		constraint4.put( "operationType", "READ");		
		constraints[4]=constraint4;
		
		
		JSONObject constraint5 = new JSONObject();
		constraint5.put("checkType","DATABASE");
		constraint5.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint5.put("isActive",true);
		constraint5.put( "entityName", "ecommerceadmin$VendorPromotion");
		constraint5.put( "operationType", "READ");		
		constraints[5]=constraint5;
		
		JSONObject constraint6 = new JSONObject();
		constraint6.put("checkType","DATABASE");
		constraint6.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint6.put("isActive",true);
		constraint6.put( "entityName", "ecommerceadmin$Order");
		constraint6.put( "operationType", "READ");		
		constraints[6]=constraint6;
		
		
		JSONObject constraint7 = new JSONObject();
		constraint7.put("checkType","DATABASE");
		constraint7.put("whereClause","{E}.vendorCompanyId= :session$vendor_company_session");
		constraint7.put("isActive",true);
		constraint7.put( "entityName", "ecommerceadmin$OrderDetails");
		constraint7.put( "operationType", "READ");		
		constraints[7]=constraint7;
		
		groupObj.put("constraints", constraints);
	
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity entity = new HttpEntity(groupObj.toString(), headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(createGroupUrl,HttpMethod.POST, entity, String.class);
			result = new JSONObject(response.getBody());
			
			System.out.println(result);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	
	
	
	
	public static JSONObject getTenantGroup() {
		JSONObject result =null;

		RestTemplate restTemplate = new RestTemplate();
		
	    HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity entity = new HttpEntity(headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(getTenantGroupUrl,HttpMethod.GET, entity, String.class);
			result = new JSONObject(response.getBody());
			//result = json.get("id_token").toString();
			//System.out.println(result);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	
	public static JSONObject getGroup(String groupId) {
		
		JSONObject result =null;

		RestTemplate restTemplate = new RestTemplate();
		
	    HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity entity = new HttpEntity(headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(getGroupUrl+groupId,HttpMethod.GET, entity, String.class);
			result = new JSONObject(response.getBody());
			//result = json.get("id_token").toString();
			//System.out.println(result);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}
	
	public static JSONObject getRole(String roleId) {
		JSONObject result =null;

		RestTemplate restTemplate = new RestTemplate();
		
	    HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + getAuthToken()); 
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity entity = new HttpEntity(headers);

		try {
			ResponseEntity<String> response = restTemplate.exchange(getRoleUrl,HttpMethod.GET, entity, String.class);
			result = new JSONObject(response.getBody());
			//result = json.get("id_token").toString();
			//System.out.println(result);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		return result;
	}

}
