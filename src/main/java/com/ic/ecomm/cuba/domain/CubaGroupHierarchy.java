package com.ic.ecomm.cuba.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "SEC_GROUP_HIERARCHY")
public class CubaGroupHierarchy {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column(name = "id")
	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
	private UUID id;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_ID")
	private CubaGroup group;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	private CubaGroup parent;

	@Column(name = "HIERARCHY_LEVEL")
	private Integer level;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public CubaGroup getGroup() {
		return group;
	}

	public void setGroup(CubaGroup group) {
		this.group = group;
	}

	public CubaGroup getParent() {
		return parent;
	}

	public void setParent(CubaGroup parent) {
		this.parent = parent;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	


}
