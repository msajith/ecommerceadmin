package com.ic.ecomm.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";
    
    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";
    
    public static final String DELIMITER ="###";
    public static final String ROLE_USER ="ROLE_USER";

	public static final String TENANT_ID = "TENANT_ID";

	public static final String VENDOR_ID = "VENDOR_ID";
	
	public static final String CLIENT_SESSION = "client_session";
	public static final String VENDOR_COMPANY_SESSION = "vendor_company_session";
	public static final String VENDOR_SESSION = "vendor_session";
	
	public static final String MERCHANT_ID = "VENDOR_COMPANY_ID";
	public static final String STORE_ID = "VENDOR_ID";

	
	//client_session , vendor_company_session,vendor_session

    private Constants() {
    }
}
