import Vue from 'vue';
import Component from 'vue-class-component';
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate' // for vue-router 2.2+
]);
import Router from 'vue-router';
const Home = () => import('../core/home/home.vue');
const Error = () => import('../core/error/error.vue');
const Register = () => import('../account/register/register.vue');
const Activate = () => import('../account/activate/activate.vue');
const ResetPasswordInit = () => import('../account/reset-password/init/reset-password-init.vue');
const ResetPasswordFinish = () => import('../account/reset-password/finish/reset-password-finish.vue');
const ChangePassword = () => import('../account/change-password/change-password.vue');
const Settings = () => import('../account/settings/settings.vue');
const JhiUserManagementComponent = () => import('../admin/user-management/user-management.vue');
const JhiUserManagementViewComponent = () => import('../admin/user-management/user-management-view.vue');
const JhiUserManagementEditComponent = () => import('../admin/user-management/user-management-edit.vue');
const JhiConfigurationComponent = () => import('../admin/configuration/configuration.vue');
const JhiDocsComponent = () => import('../admin/docs/docs.vue');
const JhiHealthComponent = () => import('../admin/health/health.vue');
const JhiLogsComponent = () => import('../admin/logs/logs.vue');
const JhiAuditsComponent = () => import('../admin/audits/audits.vue');
const JhiMetricsComponent = () => import('../admin/metrics/metrics.vue');
/* tslint:disable */
// prettier-ignore
const Category = () => import('../entities/category/category.vue');
// prettier-ignore
const CategoryUpdate = () => import('../entities/category/category-update.vue');
// prettier-ignore
const CategoryDetails = () => import('../entities/category/category-details.vue');
// prettier-ignore
const Brand = () => import('../entities/brand/brand.vue');
// prettier-ignore
const BrandUpdate = () => import('../entities/brand/brand-update.vue');
// prettier-ignore
const BrandDetails = () => import('../entities/brand/brand-details.vue');
// prettier-ignore
const ReturnPolicy = () => import('../entities/return-policy/return-policy.vue');
// prettier-ignore
const ReturnPolicyUpdate = () => import('../entities/return-policy/return-policy-update.vue');
// prettier-ignore
const ReturnPolicyDetails = () => import('../entities/return-policy/return-policy-details.vue');
// prettier-ignore
const Description = () => import('../entities/description/description.vue');
// prettier-ignore
const DescriptionUpdate = () => import('../entities/description/description-update.vue');
// prettier-ignore
const DescriptionDetails = () => import('../entities/description/description-details.vue');
// prettier-ignore
const Property = () => import('../entities/property/property.vue');
// prettier-ignore
const PropertyUpdate = () => import('../entities/property/property-update.vue');
// prettier-ignore
const PropertyDetails = () => import('../entities/property/property-details.vue');
// prettier-ignore
const ProductGroup = () => import('../entities/product-group/product-group.vue');
// prettier-ignore
const ProductGroupUpdate = () => import('../entities/product-group/product-group-update.vue');
// prettier-ignore
const ProductGroupDetails = () => import('../entities/product-group/product-group-details.vue');
// prettier-ignore
const Product = () => import('../entities/product/product.vue');
// prettier-ignore
const ProductUpdate = () => import('../entities/product/product-update.vue');
// prettier-ignore
const ProductDetails = () => import('../entities/product/product-details.vue');
// prettier-ignore
const ProductVendorClient = () => import('../entities/product-vendor-client/product-vendor-client.vue');
// prettier-ignore
const ProductVendorClientUpdate = () => import('../entities/product-vendor-client/product-vendor-client-update.vue');
// prettier-ignore
const ProductVendorClientDetails = () => import('../entities/product-vendor-client/product-vendor-client-details.vue');
// prettier-ignore
const Inventory = () => import('../entities/inventory/inventory.vue');
// prettier-ignore
const InventoryUpdate = () => import('../entities/inventory/inventory-update.vue');
// prettier-ignore
const InventoryDetails = () => import('../entities/inventory/inventory-details.vue');
// prettier-ignore
const Vendor = () => import('../entities/vendor/vendor.vue');
// prettier-ignore
const VendorUpdate = () => import('../entities/vendor/vendor-update.vue');
// prettier-ignore
const VendorDetails = () => import('../entities/vendor/vendor-details.vue');
// prettier-ignore
const Address = () => import('../entities/address/address.vue');
// prettier-ignore
const AddressUpdate = () => import('../entities/address/address-update.vue');
// prettier-ignore
const AddressDetails = () => import('../entities/address/address-details.vue');
// prettier-ignore
const OrderLineItem = () => import('../entities/order-line-item/order-line-item.vue');
// prettier-ignore
const OrderLineItemUpdate = () => import('../entities/order-line-item/order-line-item-update.vue');
// prettier-ignore
const OrderLineItemDetails = () => import('../entities/order-line-item/order-line-item-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

Vue.use(Router);

// prettier-ignore



const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forbidden',
      name: 'Forbidden',
      component: Error,
      meta: { error403: true }
    },
    {
      path: '/not-found',
      name: 'NotFound',
      component: Error,
      meta: { error404: true }
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/activate',
      name: 'Activate',
      component: Activate
    },
    {
      path: '/reset/request',
      name: 'ResetPasswordInit',
      component: ResetPasswordInit
    },
    {
      path: '/reset/finish',
      name: 'ResetPasswordFinish',
      component: ResetPasswordFinish
    },
    {
      path: '/account/password',
      name: 'ChangePassword',
      component: ChangePassword,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/account/settings',
      name: 'Settings',
      component: Settings,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/admin/user-management',
      name: 'JhiUser',
      component: JhiUserManagementComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/new',
      name: 'JhiUserCreate',
      component: JhiUserManagementEditComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/:userId/edit',
      name: 'JhiUserEdit',
      component: JhiUserManagementEditComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/:userId/view',
      name: 'JhiUserView',
      component: JhiUserManagementViewComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/docs',
      name: 'JhiDocsComponent',
      component: JhiDocsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/audits',
      name: 'JhiAuditsComponent',
      component: JhiAuditsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-health',
      name: 'JhiHealthComponent',
      component: JhiHealthComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/logs',
      name: 'JhiLogsComponent',
      component: JhiLogsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-metrics',
      name: 'JhiMetricsComponent',
      component: JhiMetricsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-configuration',
      name: 'JhiConfigurationComponent',
      component: JhiConfigurationComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    }
    ,
    {
      path: '/entity/category',
      name: 'Category',
      component: Category,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/category/new',
      name: 'CategoryCreate',
      component: CategoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/category/:categoryId/edit',
      name: 'CategoryEdit',
      component: CategoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/category/:categoryId/view',
      name: 'CategoryView',
      component: CategoryDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/brand',
      name: 'Brand',
      component: Brand,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/brand/new',
      name: 'BrandCreate',
      component: BrandUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/brand/:brandId/edit',
      name: 'BrandEdit',
      component: BrandUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/brand/:brandId/view',
      name: 'BrandView',
      component: BrandDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/return-policy',
      name: 'ReturnPolicy',
      component: ReturnPolicy,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/return-policy/new',
      name: 'ReturnPolicyCreate',
      component: ReturnPolicyUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/return-policy/:returnPolicyId/edit',
      name: 'ReturnPolicyEdit',
      component: ReturnPolicyUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/return-policy/:returnPolicyId/view',
      name: 'ReturnPolicyView',
      component: ReturnPolicyDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/description',
      name: 'Description',
      component: Description,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/description/new',
      name: 'DescriptionCreate',
      component: DescriptionUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/description/:descriptionId/edit',
      name: 'DescriptionEdit',
      component: DescriptionUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/description/:descriptionId/view',
      name: 'DescriptionView',
      component: DescriptionDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/property',
      name: 'Property',
      component: Property,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/property/new',
      name: 'PropertyCreate',
      component: PropertyUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/property/:propertyId/edit',
      name: 'PropertyEdit',
      component: PropertyUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/property/:propertyId/view',
      name: 'PropertyView',
      component: PropertyDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-group',
      name: 'ProductGroup',
      component: ProductGroup,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-group/new',
      name: 'ProductGroupCreate',
      component: ProductGroupUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-group/:productGroupId/edit',
      name: 'ProductGroupEdit',
      component: ProductGroupUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-group/:productGroupId/view',
      name: 'ProductGroupView',
      component: ProductGroupDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product',
      name: 'Product',
      component: Product,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product/new',
      name: 'ProductCreate',
      component: ProductUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product/:productId/edit',
      name: 'ProductEdit',
      component: ProductUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product/:productId/view',
      name: 'ProductView',
      component: ProductDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-vendor-client',
      name: 'ProductVendorClient',
      component: ProductVendorClient,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-vendor-client/new',
      name: 'ProductVendorClientCreate',
      component: ProductVendorClientUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-vendor-client/:productVendorClientId/edit',
      name: 'ProductVendorClientEdit',
      component: ProductVendorClientUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-vendor-client/:productVendorClientId/view',
      name: 'ProductVendorClientView',
      component: ProductVendorClientDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/inventory',
      name: 'Inventory',
      component: Inventory,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/inventory/new',
      name: 'InventoryCreate',
      component: InventoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/inventory/:inventoryId/edit',
      name: 'InventoryEdit',
      component: InventoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/inventory/:inventoryId/view',
      name: 'InventoryView',
      component: InventoryDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/vendor',
      name: 'Vendor',
      component: Vendor,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/vendor/new',
      name: 'VendorCreate',
      component: VendorUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/vendor/:vendorId/edit',
      name: 'VendorEdit',
      component: VendorUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/vendor/:vendorId/view',
      name: 'VendorView',
      component: VendorDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/address',
      name: 'Address',
      component: Address,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/address/new',
      name: 'AddressCreate',
      component: AddressUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/address/:addressId/edit',
      name: 'AddressEdit',
      component: AddressUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/address/:addressId/view',
      name: 'AddressView',
      component: AddressDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/order-line-item',
      name: 'OrderLineItem',
      component: OrderLineItem,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order-line-item/new',
      name: 'OrderLineItemCreate',
      component: OrderLineItemUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order-line-item/:orderLineItemId/edit',
      name: 'OrderLineItemEdit',
      component: OrderLineItemUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order-line-item/:orderLineItemId/view',
      name: 'OrderLineItemView',
      component: OrderLineItemDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ]
});

router.afterEach((to, from) => {
  console.log(to);
  console.log(from);

  var path = location.pathname + location.hash;
var properties = {
    path: path,
    pageInfo: {
        destinationURL: location.href,
        tags: ["tag1", "tag2", "tag3"],
        categories: ["category1", "category2", "category3"],
    },
    interests: {
        "interest1": 1,
        "interest2": 2,
        "interest3": 3
    }
};
console.log(properties);
window.unomiTracker.page(properties); 
});

export default router;
