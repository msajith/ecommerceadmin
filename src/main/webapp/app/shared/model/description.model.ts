import { IProduct } from '@/shared/model/product.model';

export interface IDescription {
  id?: string;
  description?: string;
  product?: IProduct;
}

export class Description implements IDescription {
  constructor(public id?: string, public description?: string, public product?: IProduct) {}
}
