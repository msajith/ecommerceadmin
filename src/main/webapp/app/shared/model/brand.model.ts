export interface IBrand {
  id?: string;
  brandId?: string;
  name?: string;
  brandDescription?: string;
  brandUrl?: string;
  brandLogo?: string;
}

export class Brand implements IBrand {
  constructor(public id?: string, public brandId?: string, public name?: string,brandDescription?: string,brandUrl?: string,brandLogo?: string) {}
}
