export interface IReturnPolicy {
  id?: string;
  name?: string;
  returnable?: boolean;
  description?: string;
  policyId?: string;
  clientId?: string;
}

export class ReturnPolicy implements IReturnPolicy {
  constructor(
    public id?: string,
    public name?: string,
    public returnable?: boolean,
    public description?: string,
    public policyId?: string,
    public clientId?: string
  ) {
    this.returnable = this.returnable || false;
  }
}
