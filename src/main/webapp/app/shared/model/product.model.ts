import { IProperty } from '@/shared/model/property.model';
import { IDescription } from '@/shared/model/description.model';
import { ICategory } from '@/shared/model/category.model';
import { IBrand } from '@/shared/model/brand.model';
import { IReturnPolicy } from '@/shared/model/return-policy.model';
import { IProductGroup } from '@/shared/model/product-group.model';

export interface IProduct {
  id?: string;
  clientId?: string;
  vendorCompanyId?: string;
  returnDaysLimit?: number;
  propertyCount?: number;
  descriptionCount?: number;
  productCategoryId?: string;
  vendorId?: string;
  tagCode?: string;
  modelCode?: string;
  productName?: string;
  productId?: string;
  description?: string;
  weight?: number;
  unit?: string;
  shortDescription?: string;
  image1?: string;
  image2?: string;
  image3?: string;
  image4?: string;
  backgroundImage?: string;
  status?: string;
  properties?: IProperty[];
  highlights?: IDescription[];
  category?: ICategory;
  brand?: IBrand;
  returnPolicy?: IReturnPolicy;
  productGroup?: IProductGroup;
}

export class Product implements IProduct {
  constructor(
    public id?: string,
    public clientId?: string,
    public vendorCompanyId?: string,
    public returnDaysLimit?: number,
    public propertyCount?: number,
    public descriptionCount?: number,
    public productCategoryId?: string,
    public vendorId?: string,
    public tagCode?: string,
    public modelCode?: string,
    public productName?: string,
    public productId?: string,
    public description?: string,
    public weight?: number,
    public unit?: string,
    public shortDescription?: string,
    public image1?: string,
    public image2?: string,
    public image3?: string,
    public image4?: string,
    public backgroundImage?: string,
    public status?: string,
    public properties?: IProperty[],
    public highlights?: IDescription[],
    public category?: ICategory,
    public brand?: IBrand,
    public returnPolicy?: IReturnPolicy,
    public productGroup?: IProductGroup
  ) {}
}
