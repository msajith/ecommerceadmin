import { IProduct } from '@/shared/model/product.model';

export interface IProductGroup {
  id?: string;
  productGroup?: string;
  clientId?: string;
  productGroupId?: string;
  products?: IProduct;
}

export class ProductGroup implements IProductGroup {
  constructor(
    public id?: string,
    public productGroup?: string,
    public clientId?: string,
    public productGroupId?: string,
    public products?: IProduct
  ) {}
}
