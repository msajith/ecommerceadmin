import { IProduct } from '@/shared/model/product.model';

export interface IProperty {
  id?: string;
  propertyName?: string;
  filter?: boolean;
  image?: string;
  variant?: boolean;
  propertyCount?: number;
  propertyValue?: string;
  products?: IProduct;
  product?: IProduct;
}

export class Property implements IProperty {
  constructor(
    public id?: string,
    public propertyName?: string,
    public filter?: boolean,
    public image?: string,
    public variant?: boolean,
    public propertyCount?: number,
    public propertyValue?: string,
    public products?: IProduct,
    public product?: IProduct
  ) {
    this.filter = this.filter || false;
    this.variant = this.variant || false;
  }
}
