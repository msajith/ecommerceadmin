import { ICategory } from '@/shared/model/category.model';

export interface ICategory {
  id?: string;
  categoryId?: string;
  clientId?: string;
  depthLevel?: number;
  categoryName?: string;
  categoryImage?: string;
  recordStatus?: number;
  parent?: ICategory;
}

export class Category implements ICategory {
  constructor(
    public id?: string,
    public categoryId?: string,
    public clientId?: string,
    public depthLevel?: number,
    public categoryName?: string,
    public categoryImage?: string,
    public recordStatus?: number,
    public parent?: ICategory
  ) {}
}
