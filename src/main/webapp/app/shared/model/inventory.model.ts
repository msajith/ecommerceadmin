export interface IInventory {
  id?: string;
  productName?: string;
  vendorCompanyId?: string;
  vendorName?: string;
  clientId?: string;
  vendorId?: string;
  productId?: string;
  status?: string;
  unitPrice?: number;
  costprice?: number;
  maxQuantity?: number;
  availablestock?: number;
  salesQuantity?: number;
  priceTag?: string;
  productVendorId?: string;
  productCategoryId?: string;
}

export class Inventory implements IInventory {
  constructor(
    public id?: string,
    public productName?: string,
    public vendorCompanyId?: string,
    public vendorName?: string,
    public clientId?: string,
    public vendorId?: string,
    public productId?: string,
    public status?: string,
    public unitPrice?: number,
    public costprice?: number,
    public maxQuantity?: number,
    public availablestock?: number,
    public salesQuantity?: number,
    public priceTag?: string,
    public productVendorId?: string,
    public productCategoryId?: string
  ) {}
}
