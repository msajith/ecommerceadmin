export interface IProductVendorClient {
  id?: string;
  clientId?: string;
  vendor?: string;
  productId?: string;
  productCategoryId?: string;
  productVendorId?: string;
}

export class ProductVendorClient implements IProductVendorClient {
  constructor(
    public id?: string,
    public clientId?: string,
    public vendor?: string,
    public productId?: string,
    public productCategoryId?: string,
    public productVendorId?: string
  ) {}
}
