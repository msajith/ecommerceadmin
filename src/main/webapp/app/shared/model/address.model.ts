import { IVendor } from '@/shared/model/vendor.model';

export interface IAddress {
  id?: string;
  name?: string;
  country?: string;
  state?: string;
  district?: string;
  city?: string;
  street?: string;
  vendor?: IVendor;
}

export class Address implements IAddress {
  constructor(
    public id?: string,
    public name?: string,
    public country?: string,
    public state?: string,
    public district?: string,
    public city?: string,
    public street?: string,
    public vendor?: IVendor
  ) {}
}
