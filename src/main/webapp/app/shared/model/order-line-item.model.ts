export const enum PaymentStatus {
  SUCCESS = 'SUCCESS',
  PENDING = 'PENDING',
  FAILED = 'FAILED'
}

export const enum FullOrderStatus {
  Delivered = 'Delivered',
  Shipped = 'Shipped',
  Processing = 'Processing'
}

export const enum OrderStatus {
  Delivered = 'Delivered',
  Shipped = 'Shipped',
  Processing = 'Processing'
}

export interface IOrderLineItem {
  id?: string;
  orderNumber?: string;
  vendorCompanyId?: string;
  paymentDate?: Date;
  tax?: number;
  shipmentFee?: number;
  totalAmount?: number;
  deliveredDate?: Date;
  productName?: string;
  trackingId?: string;
  trackingUrl?: string;
  clientId?: string;
  email?: string;
  resetToken?: string;
  timeStamp?: Date;
  orderDetailId?: number;
  productId?: string;
  quantity?: number;
  price?: number;
  shippedDate?: Date;
  paymentMethod?: string;
  paymentStatus?: PaymentStatus;
  orderDates?: string;
  fullOrderStatus?: FullOrderStatus;
  customerAddress?: string;
  promoCode?: string;
  appliedCouponAmount?: number;
  giftCardId?: string;
  giftCardAmount?: number;
  subTotal?: number;
  createdAt?: Date;
  updatedAt?: Date;
  customerId?: string;
  vendorId?: string;
  orderStatus?: OrderStatus;
  orderDate?: Date;
  isProgressed?: boolean;
  isShipped?: boolean;
}

export class OrderLineItem implements IOrderLineItem {
  constructor(
    public id?: string,
    public orderNumber?: string,
    public vendorCompanyId?: string,
    public paymentDate?: Date,
    public tax?: number,
    public shipmentFee?: number,
    public totalAmount?: number,
    public deliveredDate?: Date,
    public productName?: string,
    public trackingId?: string,
    public trackingUrl?: string,
    public clientId?: string,
    public email?: string,
    public resetToken?: string,
    public timeStamp?: Date,
    public orderDetailId?: number,
    public productId?: string,
    public quantity?: number,
    public price?: number,
    public shippedDate?: Date,
    public paymentMethod?: string,
    public paymentStatus?: PaymentStatus,
    public orderDates?: string,
    public fullOrderStatus?: FullOrderStatus,
    public customerAddress?: string,
    public promoCode?: string,
    public appliedCouponAmount?: number,
    public giftCardId?: string,
    public giftCardAmount?: number,
    public subTotal?: number,
    public createdAt?: Date,
    public updatedAt?: Date,
    public customerId?: string,
    public vendorId?: string,
    public orderStatus?: OrderStatus,
    public orderDate?: Date,
    public isProgressed?: boolean,
    public isShipped?: boolean
  ) {
    this.isProgressed = this.isProgressed || false;
    this.isShipped = this.isShipped || false;
  }
}
