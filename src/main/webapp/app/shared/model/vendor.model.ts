import { IAddress } from '@/shared/model/address.model';

export interface IVendor {
  id?: string;
  clientId?: string;
  cellId?: string;
  addressLine1?: string;
  addressLine2?: string;
  idProof?: string;
  document?: string;
  agentId?: string;
  maxDueAmount?: number;
  vendorEmail?: string;
  deliveryCount?: number;
  addressCount?: number;
  latitude?: string;
  longitude?: string;
  vendorClass?: string;
  vendorContact?: string;
  vendorId?: string;
  vendorName?: string;
  vendorCompanyName?: string;
  vendorCompanyId?: string;
  country?: string;
  state?: string;
  district?: string;
  addresses?: IAddress[];
}

export class Vendor implements IVendor {
  constructor(
    public id?: string,
    public clientId?: string,
    public cellId?: string,
    public addressLine1?: string,
    public addressLine2?: string,
    public idProof?: string,
    public document?: string,
    public agentId?: string,
    public maxDueAmount?: number,
    public vendorEmail?: string,
    public deliveryCount?: number,
    public addressCount?: number,
    public latitude?: string,
    public longitude?: string,
    public vendorClass?: string,
    public vendorContact?: string,
    public vendorId?: string,
    public vendorName?: string,
    public vendorCompanyName?: string,
    public vendorCompanyId?: string,
    public country?: string,
    public state?: string,
    public district?: string,
    public addresses?: IAddress[]
  ) {}
}
