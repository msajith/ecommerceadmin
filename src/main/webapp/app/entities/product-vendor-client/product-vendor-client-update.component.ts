import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IProductVendorClient, ProductVendorClient } from '@/shared/model/product-vendor-client.model';
import ProductVendorClientService from './product-vendor-client.service';

const validations: any = {
  productVendorClient: {
    clientId: {},
    vendor: {},
    productId: {},
    productCategoryId: {},
    productVendorId: {}
  }
};

@Component({
  validations
})
export default class ProductVendorClientUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productVendorClientService') private productVendorClientService: () => ProductVendorClientService;
  public productVendorClient: IProductVendorClient = new ProductVendorClient();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productVendorClientId) {
        vm.retrieveProductVendorClient(to.params.productVendorClientId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.productVendorClient.id) {
      this.productVendorClientService()
        .update(this.productVendorClient)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.productVendorClient.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productVendorClientService()
        .create(this.productVendorClient)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.productVendorClient.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProductVendorClient(productVendorClientId): void {
    this.productVendorClientService()
      .find(productVendorClientId)
      .then(res => {
        this.productVendorClient = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
