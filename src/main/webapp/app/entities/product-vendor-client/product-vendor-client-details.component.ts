import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProductVendorClient } from '@/shared/model/product-vendor-client.model';
import ProductVendorClientService from './product-vendor-client.service';

@Component
export default class ProductVendorClientDetails extends Vue {
  @Inject('productVendorClientService') private productVendorClientService: () => ProductVendorClientService;
  public productVendorClient: IProductVendorClient = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productVendorClientId) {
        vm.retrieveProductVendorClient(to.params.productVendorClientId);
      }
    });
  }

  public retrieveProductVendorClient(productVendorClientId) {
    this.productVendorClientService()
      .find(productVendorClientId)
      .then(res => {
        this.productVendorClient = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
