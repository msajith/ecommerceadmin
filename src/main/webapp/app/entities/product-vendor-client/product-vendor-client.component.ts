import { Component, Inject, Vue } from 'vue-property-decorator';
import { IProductVendorClient } from '@/shared/model/product-vendor-client.model';
import AlertService from '@/shared/alert/alert.service';

import ProductVendorClientService from './product-vendor-client.service';

@Component
export default class ProductVendorClient extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productVendorClientService') private productVendorClientService: () => ProductVendorClientService;
  private removeId: string = null;
  public productVendorClients: IProductVendorClient[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllProductVendorClients();
  }

  public clear(): void {
    this.retrieveAllProductVendorClients();
  }

  public retrieveAllProductVendorClients(): void {
    this.isFetching = true;

    this.productVendorClientService()
      .retrieve()
      .then(
        res => {
          this.productVendorClients = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IProductVendorClient): void {
    this.removeId = instance.id;
  }

  public removeProductVendorClient(): void {
    this.productVendorClientService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.productVendorClient.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllProductVendorClients();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
