import axios from 'axios';

import { IProductVendorClient } from '@/shared/model/product-vendor-client.model';

const baseApiUrl = 'api/product-vendor-clients';

export default class ProductVendorClientService {
  public find(id: number): Promise<IProductVendorClient> {
    return new Promise<IProductVendorClient>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IProductVendorClient): Promise<IProductVendorClient> {
    return new Promise<IProductVendorClient>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IProductVendorClient): Promise<IProductVendorClient> {
    return new Promise<IProductVendorClient>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
