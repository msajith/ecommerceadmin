import { Component, Vue, Inject } from 'vue-property-decorator';

import { IOrderLineItem } from '@/shared/model/order-line-item.model';
import OrderLineItemService from './order-line-item.service';

@Component
export default class OrderLineItemDetails extends Vue {
  @Inject('orderLineItemService') private orderLineItemService: () => OrderLineItemService;
  public orderLineItem: IOrderLineItem = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderLineItemId) {
        vm.retrieveOrderLineItem(to.params.orderLineItemId);
      }
    });
  }

  public retrieveOrderLineItem(orderLineItemId) {
    this.orderLineItemService()
      .find(orderLineItemId)
      .then(res => {
        this.orderLineItem = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
