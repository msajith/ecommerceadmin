import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import { DATE_TIME_LONG_FORMAT, INSTANT_FORMAT, ZONED_DATE_TIME_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import { IOrderLineItem, OrderLineItem } from '@/shared/model/order-line-item.model';
import OrderLineItemService from './order-line-item.service';

const validations: any = {
  orderLineItem: {
    orderNumber: {},
    vendorCompanyId: {},
    paymentDate: {},
    tax: {},
    shipmentFee: {},
    totalAmount: {},
    deliveredDate: {},
    productName: {},
    trackingId: {},
    trackingUrl: {},
    clientId: {},
    email: {},
    resetToken: {},
    timeStamp: {},
    orderDetailId: {},
    productId: {},
    quantity: {},
    price: {},
    shippedDate: {},
    paymentMethod: {},
    paymentStatus: {},
    orderDates: {},
    fullOrderStatus: {},
    customerAddress: {},
    promoCode: {},
    appliedCouponAmount: {},
    giftCardId: {},
    giftCardAmount: {},
    subTotal: {},
    createdAt: {},
    updatedAt: {},
    customerId: {},
    vendorId: {},
    orderStatus: {},
    orderDate: {},
    isProgressed: {},
    isShipped: {}
  }
};

@Component({
  validations
})
export default class OrderLineItemUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('orderLineItemService') private orderLineItemService: () => OrderLineItemService;
  public orderLineItem: IOrderLineItem = new OrderLineItem();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderLineItemId) {
        vm.retrieveOrderLineItem(to.params.orderLineItemId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.orderLineItem.id) {
      this.orderLineItemService()
        .update(this.orderLineItem)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.orderLineItem.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.orderLineItemService()
        .create(this.orderLineItem)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.orderLineItem.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.orderLineItem[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.orderLineItem[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.orderLineItem[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.orderLineItem[field] = null;
    }
  }

  public retrieveOrderLineItem(orderLineItemId): void {
    this.orderLineItemService()
      .find(orderLineItemId)
      .then(res => {
        this.orderLineItem = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
