import axios from 'axios';

import { IOrderLineItem } from '@/shared/model/order-line-item.model';

const baseApiUrl = 'api/order-line-items';

export default class OrderLineItemService {
  public find(id: number): Promise<IOrderLineItem> {
    return new Promise<IOrderLineItem>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IOrderLineItem): Promise<IOrderLineItem> {
    return new Promise<IOrderLineItem>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IOrderLineItem): Promise<IOrderLineItem> {
    return new Promise<IOrderLineItem>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
