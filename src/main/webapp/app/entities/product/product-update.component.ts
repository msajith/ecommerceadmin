import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import PropertyService from '../property/property.service';
import { IProperty } from '@/shared/model/property.model';

import DescriptionService from '../description/description.service';
import { IDescription } from '@/shared/model/description.model';

import CategoryService from '../category/category.service';
import { ICategory } from '@/shared/model/category.model';

import BrandService from '../brand/brand.service';
import { IBrand } from '@/shared/model/brand.model';

import ReturnPolicyService from '../return-policy/return-policy.service';
import { IReturnPolicy } from '@/shared/model/return-policy.model';

import ProductGroupService from '../product-group/product-group.service';
import { IProductGroup } from '@/shared/model/product-group.model';

import AlertService from '@/shared/alert/alert.service';
import { IProduct, Product } from '@/shared/model/product.model';
import ProductService from './product.service';

const validations: any = {
  product: {
    clientId: {},
    vendorCompanyId: {},
    returnDaysLimit: {},
    propertyCount: {},
    descriptionCount: {},
    productCategoryId: {},
    vendorId: {},
    tagCode: {},
    modelCode: {},
    productName: {},
    productId: {},
    description: {},
    weight: {},
    unit: {},
    shortDescription: {},
    image1: {},
    image2: {},
    image3: {},
    image4: {},
    backgroundImage: {},
    status: {}
  }
};

@Component({
  validations
})
export default class ProductUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productService') private productService: () => ProductService;
  public product: IProduct = new Product();

  @Inject('propertyService') private propertyService: () => PropertyService;

  public properties: IProperty[] = [];

  @Inject('descriptionService') private descriptionService: () => DescriptionService;

  public descriptions: IDescription[] = [];

  @Inject('categoryService') private categoryService: () => CategoryService;

  public categories: ICategory[] = [];

  @Inject('brandService') private brandService: () => BrandService;

  public brands: IBrand[] = [];

  @Inject('returnPolicyService') private returnPolicyService: () => ReturnPolicyService;

  public returnPolicies: IReturnPolicy[] = [];

  @Inject('productGroupService') private productGroupService: () => ProductGroupService;

  public productGroups: IProductGroup[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productId) {
        vm.retrieveProduct(to.params.productId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.product.id) {
      this.productService()
        .update(this.product)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.product.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productService()
        .create(this.product)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.product.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProduct(productId): void {
    this.productService()
      .find(productId)
      .then(res => {
        this.product = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.propertyService()
      .retrieve()
      .then(res => {
        this.properties = res.data;
      });
    this.descriptionService()
      .retrieve()
      .then(res => {
        this.descriptions = res.data;
      });
    this.categoryService()
      .retrieve()
      .then(res => {
        this.categories = res.data;
      });
    this.brandService()
      .retrieve()
      .then(res => {
        this.brands = res.data;
      });
    this.returnPolicyService()
      .retrieve()
      .then(res => {
        this.returnPolicies = res.data;
      });
    this.productGroupService()
      .retrieve()
      .then(res => {
        this.productGroups = res.data;
      });
  }
}
