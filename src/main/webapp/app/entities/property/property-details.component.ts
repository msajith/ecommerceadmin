import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProperty } from '@/shared/model/property.model';
import PropertyService from './property.service';

@Component
export default class PropertyDetails extends Vue {
  @Inject('propertyService') private propertyService: () => PropertyService;
  public property: IProperty = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.propertyId) {
        vm.retrieveProperty(to.params.propertyId);
      }
    });
  }

  public retrieveProperty(propertyId) {
    this.propertyService()
      .find(propertyId)
      .then(res => {
        this.property = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
