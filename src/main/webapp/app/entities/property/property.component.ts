import { Component, Inject, Vue } from 'vue-property-decorator';
import { IProperty } from '@/shared/model/property.model';
import AlertService from '@/shared/alert/alert.service';

import PropertyService from './property.service';

@Component
export default class Property extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('propertyService') private propertyService: () => PropertyService;
  private removeId: string = null;
  public properties: IProperty[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllPropertys();
  }

  public clear(): void {
    this.retrieveAllPropertys();
  }

  public retrieveAllPropertys(): void {
    this.isFetching = true;

    this.propertyService()
      .retrieve()
      .then(
        res => {
          this.properties = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IProperty): void {
    this.removeId = instance.id;
  }

  public removeProperty(): void {
    this.propertyService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.property.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllPropertys();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
