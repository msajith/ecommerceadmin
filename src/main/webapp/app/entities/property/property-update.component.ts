import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import ProductService from '../product/product.service';
import { IProduct } from '@/shared/model/product.model';

import AlertService from '@/shared/alert/alert.service';
import { IProperty, Property } from '@/shared/model/property.model';
import PropertyService from './property.service';

const validations: any = {
  property: {
    propertyName: {},
    filter: {},
    image: {},
    variant: {},
    propertyCount: {},
    propertyValue: {}
  }
};

@Component({
  validations
})
export default class PropertyUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('propertyService') private propertyService: () => PropertyService;
  public property: IProperty = new Property();

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.propertyId) {
        vm.retrieveProperty(to.params.propertyId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.property.id) {
      this.propertyService()
        .update(this.property)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.property.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.propertyService()
        .create(this.property)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.property.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProperty(propertyId): void {
    this.propertyService()
      .find(propertyId)
      .then(res => {
        this.property = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
