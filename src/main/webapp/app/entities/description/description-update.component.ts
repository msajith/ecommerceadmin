import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import ProductService from '../product/product.service';
import { IProduct } from '@/shared/model/product.model';

import AlertService from '@/shared/alert/alert.service';
import { IDescription, Description } from '@/shared/model/description.model';
import DescriptionService from './description.service';

const validations: any = {
  description: {
    description: {}
  }
};

@Component({
  validations
})
export default class DescriptionUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('descriptionService') private descriptionService: () => DescriptionService;
  public description: IDescription = new Description();

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.descriptionId) {
        vm.retrieveDescription(to.params.descriptionId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.description.id) {
      this.descriptionService()
        .update(this.description)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.description.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.descriptionService()
        .create(this.description)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.description.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveDescription(descriptionId): void {
    this.descriptionService()
      .find(descriptionId)
      .then(res => {
        this.description = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
