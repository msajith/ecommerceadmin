import { Component, Inject, Vue } from 'vue-property-decorator';
import { IDescription } from '@/shared/model/description.model';
import AlertService from '@/shared/alert/alert.service';

import DescriptionService from './description.service';

@Component
export default class Description extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('descriptionService') private descriptionService: () => DescriptionService;
  private removeId: string = null;
  public descriptions: IDescription[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllDescriptions();
  }

  public clear(): void {
    this.retrieveAllDescriptions();
  }

  public retrieveAllDescriptions(): void {
    this.isFetching = true;

    this.descriptionService()
      .retrieve()
      .then(
        res => {
          this.descriptions = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IDescription): void {
    this.removeId = instance.id;
  }

  public removeDescription(): void {
    this.descriptionService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.description.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllDescriptions();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
