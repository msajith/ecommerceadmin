import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDescription } from '@/shared/model/description.model';
import DescriptionService from './description.service';

@Component
export default class DescriptionDetails extends Vue {
  @Inject('descriptionService') private descriptionService: () => DescriptionService;
  public description: IDescription = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.descriptionId) {
        vm.retrieveDescription(to.params.descriptionId);
      }
    });
  }

  public retrieveDescription(descriptionId) {
    this.descriptionService()
      .find(descriptionId)
      .then(res => {
        this.description = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
