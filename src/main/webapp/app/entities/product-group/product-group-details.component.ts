import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProductGroup } from '@/shared/model/product-group.model';
import ProductGroupService from './product-group.service';

@Component
export default class ProductGroupDetails extends Vue {
  @Inject('productGroupService') private productGroupService: () => ProductGroupService;
  public productGroup: IProductGroup = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productGroupId) {
        vm.retrieveProductGroup(to.params.productGroupId);
      }
    });
  }

  public retrieveProductGroup(productGroupId) {
    this.productGroupService()
      .find(productGroupId)
      .then(res => {
        this.productGroup = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
