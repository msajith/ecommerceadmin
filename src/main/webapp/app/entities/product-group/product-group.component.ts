import { Component, Inject, Vue } from 'vue-property-decorator';
import { IProductGroup } from '@/shared/model/product-group.model';
import AlertService from '@/shared/alert/alert.service';

import ProductGroupService from './product-group.service';

@Component
export default class ProductGroup extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productGroupService') private productGroupService: () => ProductGroupService;
  private removeId: string = null;
  public productGroups: IProductGroup[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllProductGroups();
  }

  public clear(): void {
    this.retrieveAllProductGroups();
  }

  public retrieveAllProductGroups(): void {
    this.isFetching = true;

    this.productGroupService()
      .retrieve()
      .then(
        res => {
          this.productGroups = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IProductGroup): void {
    this.removeId = instance.id;
  }

  public removeProductGroup(): void {
    this.productGroupService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.productGroup.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllProductGroups();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
