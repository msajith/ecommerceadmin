import axios from 'axios';

import { IProductGroup } from '@/shared/model/product-group.model';

const baseApiUrl = 'api/product-groups';

export default class ProductGroupService {
  public find(id: number): Promise<IProductGroup> {
    return new Promise<IProductGroup>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IProductGroup): Promise<IProductGroup> {
    return new Promise<IProductGroup>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IProductGroup): Promise<IProductGroup> {
    return new Promise<IProductGroup>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
