import { Component, Inject, Vue } from 'vue-property-decorator';
import { IBrand } from '@/shared/model/brand.model';
import AlertService from '@/shared/alert/alert.service';

import BrandService from './brand.service';

@Component
export default class Brand extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('brandService') private brandService: () => BrandService;
  private removeId: string = null;
  public brands: IBrand[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllBrands();
  }

  public clear(): void {
    this.retrieveAllBrands();
  }

  public retrieveAllBrands(): void {
    this.isFetching = true;

    this.brandService()
      .retrieve()
      .then(
        res => {
          this.brands = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IBrand): void {
    this.removeId = instance.id;
  }

  public removeBrand(): void {
    this.brandService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.brand.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllBrands();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
