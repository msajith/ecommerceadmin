import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IBrand, Brand } from '@/shared/model/brand.model';
import BrandService from './brand.service';

const validations: any = {
  brand: {
    brandId: {},
    name: {}
  }
};

@Component({
  validations
})
export default class BrandUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('brandService') private brandService: () => BrandService;
  public brand: IBrand = new Brand();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.brandId) {
        vm.retrieveBrand(to.params.brandId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.brand.id) {
      this.brandService()
        .update(this.brand)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.brand.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.brandService()
        .create(this.brand)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.brand.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveBrand(brandId): void {
    this.brandService()
      .find(brandId)
      .then(res => {
        this.brand = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
