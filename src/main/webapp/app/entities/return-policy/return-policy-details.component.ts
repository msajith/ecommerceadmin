import { Component, Vue, Inject } from 'vue-property-decorator';

import { IReturnPolicy } from '@/shared/model/return-policy.model';
import ReturnPolicyService from './return-policy.service';

@Component
export default class ReturnPolicyDetails extends Vue {
  @Inject('returnPolicyService') private returnPolicyService: () => ReturnPolicyService;
  public returnPolicy: IReturnPolicy = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.returnPolicyId) {
        vm.retrieveReturnPolicy(to.params.returnPolicyId);
      }
    });
  }

  public retrieveReturnPolicy(returnPolicyId) {
    this.returnPolicyService()
      .find(returnPolicyId)
      .then(res => {
        this.returnPolicy = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
