import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IReturnPolicy, ReturnPolicy } from '@/shared/model/return-policy.model';
import ReturnPolicyService from './return-policy.service';

const validations: any = {
  returnPolicy: {
    name: {},
    returnable: {},
    description: {},
    policyId: {},
    clientId: {}
  }
};

@Component({
  validations
})
export default class ReturnPolicyUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('returnPolicyService') private returnPolicyService: () => ReturnPolicyService;
  public returnPolicy: IReturnPolicy = new ReturnPolicy();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.returnPolicyId) {
        vm.retrieveReturnPolicy(to.params.returnPolicyId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.returnPolicy.id) {
      this.returnPolicyService()
        .update(this.returnPolicy)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.returnPolicy.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.returnPolicyService()
        .create(this.returnPolicy)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.returnPolicy.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveReturnPolicy(returnPolicyId): void {
    this.returnPolicyService()
      .find(returnPolicyId)
      .then(res => {
        this.returnPolicy = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
