import axios from 'axios';

import { IReturnPolicy } from '@/shared/model/return-policy.model';

const baseApiUrl = 'api/return-policies';

export default class ReturnPolicyService {
  public find(id: number): Promise<IReturnPolicy> {
    return new Promise<IReturnPolicy>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IReturnPolicy): Promise<IReturnPolicy> {
    return new Promise<IReturnPolicy>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IReturnPolicy): Promise<IReturnPolicy> {
    return new Promise<IReturnPolicy>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
