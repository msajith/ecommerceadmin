import { Component, Inject, Vue } from 'vue-property-decorator';
import { IReturnPolicy } from '@/shared/model/return-policy.model';
import AlertService from '@/shared/alert/alert.service';

import ReturnPolicyService from './return-policy.service';

@Component
export default class ReturnPolicy extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('returnPolicyService') private returnPolicyService: () => ReturnPolicyService;
  private removeId: string = null;
  public returnPolicies: IReturnPolicy[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllReturnPolicys();
  }

  public clear(): void {
    this.retrieveAllReturnPolicys();
  }

  public retrieveAllReturnPolicys(): void {
    this.isFetching = true;

    this.returnPolicyService()
      .retrieve()
      .then(
        res => {
          this.returnPolicies = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IReturnPolicy): void {
    this.removeId = instance.id;
  }

  public removeReturnPolicy(): void {
    this.returnPolicyService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.returnPolicy.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllReturnPolicys();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
