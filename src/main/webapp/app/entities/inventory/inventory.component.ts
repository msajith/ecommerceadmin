import { Component, Inject, Vue } from 'vue-property-decorator';
import { IInventory } from '@/shared/model/inventory.model';
import AlertService from '@/shared/alert/alert.service';

import InventoryService from './inventory.service';

@Component
export default class Inventory extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('inventoryService') private inventoryService: () => InventoryService;
  private removeId: string = null;
  public inventories: IInventory[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllInventorys();
  }

  public clear(): void {
    this.retrieveAllInventorys();
  }

  public retrieveAllInventorys(): void {
    this.isFetching = true;

    this.inventoryService()
      .retrieve()
      .then(
        res => {
          this.inventories = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IInventory): void {
    this.removeId = instance.id;
  }

  public removeInventory(): void {
    this.inventoryService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.inventory.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllInventorys();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
