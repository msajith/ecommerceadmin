import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IInventory, Inventory } from '@/shared/model/inventory.model';
import InventoryService from './inventory.service';

const validations: any = {
  inventory: {
    productName: {},
    vendorCompanyId: {},
    vendorName: {},
    clientId: {},
    vendorId: {},
    productId: {},
    status: {},
    unitPrice: {},
    costprice: {},
    maxQuantity: {},
    availablestock: {},
    salesQuantity: {},
    priceTag: {},
    productVendorId: {},
    productCategoryId: {}
  }
};

@Component({
  validations
})
export default class InventoryUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('inventoryService') private inventoryService: () => InventoryService;
  public inventory: IInventory = new Inventory();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.inventoryId) {
        vm.retrieveInventory(to.params.inventoryId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.inventory.id) {
      this.inventoryService()
        .update(this.inventory)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.inventory.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.inventoryService()
        .create(this.inventory)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.inventory.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveInventory(inventoryId): void {
    this.inventoryService()
      .find(inventoryId)
      .then(res => {
        this.inventory = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
