import { Component, Vue, Inject } from 'vue-property-decorator';

import { IInventory } from '@/shared/model/inventory.model';
import InventoryService from './inventory.service';

@Component
export default class InventoryDetails extends Vue {
  @Inject('inventoryService') private inventoryService: () => InventoryService;
  public inventory: IInventory = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.inventoryId) {
        vm.retrieveInventory(to.params.inventoryId);
      }
    });
  }

  public retrieveInventory(inventoryId) {
    this.inventoryService()
      .find(inventoryId)
      .then(res => {
        this.inventory = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
