import { Component, Inject, Vue } from 'vue-property-decorator';
import { IVendor } from '@/shared/model/vendor.model';
import AlertService from '@/shared/alert/alert.service';

import VendorService from './vendor.service';

@Component
export default class Vendor extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('vendorService') private vendorService: () => VendorService;
  private removeId: string = null;
  public vendors: IVendor[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllVendors();
  }

  public clear(): void {
    this.retrieveAllVendors();
  }

  public retrieveAllVendors(): void {
    this.isFetching = true;

    this.vendorService()
      .retrieve()
      .then(
        res => {
          this.vendors = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IVendor): void {
    this.removeId = instance.id;
  }

  public removeVendor(): void {
    this.vendorService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerceadminApp.vendor.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllVendors();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
