import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AddressService from '../address/address.service';
import { IAddress } from '@/shared/model/address.model';

import AlertService from '@/shared/alert/alert.service';
import { IVendor, Vendor } from '@/shared/model/vendor.model';
import VendorService from './vendor.service';

const validations: any = {
  vendor: {
    clientId: {},
    cellId: {},
    addressLine1: {},
    addressLine2: {},
    idProof: {},
    document: {},
    agentId: {},
    maxDueAmount: {},
    vendorEmail: {},
    deliveryCount: {},
    addressCount: {},
    latitude: {},
    longitude: {},
    vendorClass: {},
    vendorContact: {},
    vendorId: {},
    vendorName: {},
    vendorCompanyName: {},
    vendorCompanyId: {},
    country: {},
    state: {},
    district: {}
  }
};

@Component({
  validations
})
export default class VendorUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('vendorService') private vendorService: () => VendorService;
  public vendor: IVendor = new Vendor();

  @Inject('addressService') private addressService: () => AddressService;

  public addresses: IAddress[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vendorId) {
        vm.retrieveVendor(to.params.vendorId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.vendor.id) {
      this.vendorService()
        .update(this.vendor)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.vendor.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.vendorService()
        .create(this.vendor)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerceadminApp.vendor.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveVendor(vendorId): void {
    this.vendorService()
      .find(vendorId)
      .then(res => {
        this.vendor = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.addressService()
      .retrieve()
      .then(res => {
        this.addresses = res.data;
      });
  }
}
